﻿using AuthenticationService.Auth;
using Interfaces.Wcf;
using Models.Dtos;
using System.ServiceModel.Activation;
using System.Web;

namespace AuthenticationService
{
    public class WcfCoreService : IWcfAuthenticationService
    {
        public bool IsAuthorized()
        {
            var jwt = HttpContext.Current.Request.Cookies.Get("jwt");
            return JwtAuth.IsUser(jwt);
        }

        public bool IsAdmin()
        {
            var jwt = HttpContext.Current.Request.Cookies.Get("jwt");
            return JwtAuth.IsAdmin(jwt);
        }

        public bool IsGuest()
        {
            var jwt = HttpContext.Current.Request.Cookies.Get("jwt");
            return JwtAuth.IsGuest(jwt);
        }

        public string CreateToken(JwtPayload payload)
        {
            var token = Jwt.CreateToken(payload);
            return token;
        }

        public JwtPayload GetCurrentPayload()
        {
            var jwt = HttpContext.Current.Request.Cookies.Get("jwt");
            var payload = Jwt.DecodeToken(jwt.Value);
            return payload;
        }
    }
}
