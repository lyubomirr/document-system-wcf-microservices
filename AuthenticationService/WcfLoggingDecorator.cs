﻿using Interfaces.Wcf;
using log4net;
using Models.Dtos;
using System;
using System.ServiceModel.Web;

namespace AuthenticationService
{
    public class WcfLoggingDecorator : IWcfAuthenticationService
    {
        private IWcfAuthenticationService _wcfCore;
        private readonly ILog _logger;

        public WcfLoggingDecorator(IWcfAuthenticationService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public string CreateToken(JwtPayload payload)
        {
            _logger.Debug($"CreateToken operation called with payload: {{{payload}}}.");
            try
            {
                return _wcfCore.CreateToken(payload);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at CreateToken operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at CreateToken operation!", ex);
                return null;
            }
        }

        public JwtPayload GetCurrentPayload()
        {
            _logger.Debug($"GetCurrentPayload operation called.");
            try
            {
                return _wcfCore.GetCurrentPayload();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetCurrentPayload operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetCurrentPayload operation!", ex);
                return null;
            }
        }

        public bool IsAuthorized()
        {
            _logger.Debug($"isAuthorized operation called.");
            try
            {
                return _wcfCore.IsAuthorized();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at isAuthorized operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at isAuthorized operation!", ex);
                return false;
            }
        }

        public bool IsGuest()
        {
            _logger.Debug($"IsGuest operation called.");
            try
            {
                return _wcfCore.IsGuest();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at IsGuest operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at IsGuest operation!", ex);
                return false;
            }
        }

        public bool IsAdmin()
        {
            _logger.Debug($"isAdmin operation called.");
            try
            {
                return _wcfCore.IsAdmin();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at isAdmin operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at isAdmin operation!", ex);
                return false;
            }
        }
    }
}