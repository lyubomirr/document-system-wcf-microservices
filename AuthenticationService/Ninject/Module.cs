﻿using Interfaces.Wcf;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticationService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IWcfAuthenticationService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>();
        }
    }
}