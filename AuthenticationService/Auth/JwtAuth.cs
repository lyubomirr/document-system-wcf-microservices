﻿using JWT;
using System.Web;

namespace AuthenticationService.Auth
{
    public static class JwtAuth
    {
        public static bool IsUser(HttpCookie jwt)
        {
            try
            {
                if (jwt == null)
                {
                    return false;
                }

                Jwt.DecodeToken(jwt.Value);
                return true;
            }
            catch (TokenExpiredException)
            {
                return false;
            }
            catch (SignatureVerificationException)
            {
                return false;
            }
        }

        public static bool IsGuest(HttpCookie jwt)
        {
            var payload = Jwt.DecodeToken(jwt.Value);
            return payload.Role == "Guest";
        }

        public static bool IsAdmin(HttpCookie jwt)
        {
            var payload = Jwt.DecodeToken(jwt.Value);
            return payload.Role == "Admin";
        }
    }
}