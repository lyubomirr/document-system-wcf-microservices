﻿using Common.Factories;
using DocumentsService.Repositories;
using DocumentsService.Services;
using Interfaces.Repositories;
using Interfaces.Services;
using Interfaces.Wcf;
using Ninject.Modules;
using System.Configuration;

namespace DocumentsService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            var uploadsPath = ConfigurationManager.AppSettings["uploadsPath"];
            var tempPath = ConfigurationManager.AppSettings["tempPath"];

            Bind<IDocumentsService>().To<ApplicationService>()
                .WithConstructorArgument("uploadsDirectoryPath", uploadsPath)
                .WithConstructorArgument("tempFilesDirectoryPath", tempPath);


            Bind<IDocumentsRepository>().To<DocumentsRepository>()
               .WithConstructorArgument("tableName", "Documents");

            Bind<IWcfDocumentsService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>();

            var channelFactory = new WcfServiceChannelFactory();
            Bind<IWcfAuthenticationService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfAuthenticationService>("AuthEndpoint"));

            Bind<IWcfFileCabinetsService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfFileCabinetsService>("FileCabinetsServiceEndpoint"));

            Bind<IWcfUsersService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfUsersService>("UsersServiceEndpoint"));
        }
    }
}