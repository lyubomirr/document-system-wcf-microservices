﻿using Interfaces.Wcf;
using log4net;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace DocumentsService
{
    public class WcfLoggingDecorator : IWcfDocumentsService
    {
        private IWcfDocumentsService _wcfCore;
        private readonly ILog _logger;

        public WcfLoggingDecorator(IWcfDocumentsService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public Document Add(Document doc)
        {
            _logger.Debug($"Add operation called with parameter doc: {{{doc}}}.");
            try
            {
                return _wcfCore.Add(doc);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Add operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Add operation!", ex);
                return null;
            }
        }

        public void Archivate(string guid)
        {
            _logger.Debug($"Archivate operation called with parameter guid: \"{guid}\".");
            try
            {
                _wcfCore.Archivate(guid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Archivate operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Archivate operation!", ex);
            }
        }

        public void ArchivateFileCabinet(string fileCabinetGuid)
        {
            _logger.Debug($"ArchivateFileCabinet operation called with parameter fileCabinetGuid: \"{fileCabinetGuid}\".");
            try
            {
                _wcfCore.ArchivateFileCabinet(fileCabinetGuid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at ArchivateFileCabinet operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at ArchivateFileCabinet operation!", ex);
            }
        }

        public IList<Document> CompleteImport(UploadedFileInfo importInfo)
        {
            _logger.Debug($"CompleteImport operation called with parameter importInfo: {{{importInfo}}}.");
            try
            {
                return _wcfCore.CompleteImport(importInfo);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at CompleteImport operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at CompleteImport operation!", ex);
                return null;
            }
        }

        public string CompleteUpload(UploadedFileInfo fileInfo)
        {
            _logger.Debug($"CompleteUpload operation called with parameter fileInfo: {{{fileInfo}}}.");
            try
            {
                return _wcfCore.CompleteUpload(fileInfo);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at CompleteUpload operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at CompleteUpload operation!", ex);
                return null;
            }
        }

        public Document Copy(Document newDocument)
        {
            _logger.Debug($"Copy operation called with parameter newDocument: {{{newDocument}}}.");
            try
            {
                return _wcfCore.Copy(newDocument);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Copy operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Copy operation!", ex);
                return null;
            }
        }

        public void Delete(IEnumerable<string> guids)
        {
            _logger.Debug($"Delete operation called with parameter guids: {string.Join(",", guids)}.");
            try
            {
                _wcfCore.Delete(guids);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Delete operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Delete operation!", ex);
            }
        }

        public IList<Document> GetArchive(SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetArchive operation called with parameter searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetArchive(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetArchive operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetArchive operation!", ex);
                return null;
            }
        }

        public int GetArchiveCount(SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetArchiveCount operation called with parameter searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetArchiveCount(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetArchiveCount operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetArchiveCount operation!", ex);
                return 0;
            }
        }

        public int GetCount(string fcGuid, SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetArchiveCount operation called with parameters fcGuid: \"{fcGuid}\", searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetCount(fcGuid, searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetCount operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetCount operation!", ex);
                return 0;
            }
        }

        public IList<Document> GetDeleted(SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetDeleted operation called with parameter searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetDeleted(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetDeleted operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetDeleted operation!", ex);
                return null;
            }
        }

        public int GetDeletedCount(SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetDeletedCount operation called with parameter searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetDeletedCount(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetCount operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetCount operation!", ex);
                return 0;
            }
        }

        public Document GetDocumentById(string guid)
        {
            _logger.Debug($"GetDocumentById operation called with parameter guid: \"{guid}\" .");
            try
            {
                return _wcfCore.GetDocumentById(guid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetDocumentById operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetDocumentById operation!", ex);
                return null;
            }
        }

        public IList<Document> GetDocuments(string fcGuid, SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetDocuments operation called with parameters fcGuid: \"{fcGuid}\",searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetDocuments(fcGuid, searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetDocuments operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetDocuments operation!", ex);
                return null;
            }
        }

        public Guid GetRandomGuid()
        {
            _logger.Debug($"GetRandomGuid operation called.");
            try
            {
                return _wcfCore.GetRandomGuid();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetRandomGuid operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetRandomGuid operation!", ex);
                return Guid.Empty;
            }
        }

        public void RestoreAllArchivated()
        {
            _logger.Debug($"RestoreAllArchivated operation called.");
            try
            {
                _wcfCore.RestoreAllArchivated();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at RestoreAllArchivated operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at RestoreAllArchivated operation!", ex);
            }
        }

        public void RestoreAllDeleted()
        {
            _logger.Debug($"RestoreAllDeleted operation called.");
            try
            {
                _wcfCore.RestoreAllDeleted();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at RestoreAllDeleted operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at RestoreAllDeleted operation!", ex);
            }
        }

        public void RestoreArchivated(IEnumerable<string> guids)
        {
            _logger.Debug($"RestoreArchivated operation called with parameter guids: {string.Join(",", guids)}.");
            try
            {
                _wcfCore.RestoreArchivated(guids);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at RestoreArchivated operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at RestoreArchivated operation!", ex);
            }
        }

        public void RestoreDeleted(IEnumerable<string> guids)
        {
            _logger.Debug($"RestoreDeleted operation called with parameter guids: {string.Join(",",guids)}.");
            try
            {
                _wcfCore.RestoreDeleted(guids);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at RestoreDeleted operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at RestoreDeleted operation!", ex);
            }
        }

        public void Update(Document doc)
        {
            _logger.Debug($"Update operation called with parameter doc: {{{doc}}}.");
            try
            {
                _wcfCore.Update(doc);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Update operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Update operation!", ex);
            }
        }

        public void UploadChunks(string guid, string fileName, byte[] chunk)
        {
            _logger.Debug($"UploadChunks operation called with parameters guid: \"{guid}\", fileName: \"{fileName}\".");
            try
            {
                _wcfCore.UploadChunks(guid, fileName, chunk);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at UploadChunks operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at UploadChunks operation!", ex);
            }
        }
    }
}