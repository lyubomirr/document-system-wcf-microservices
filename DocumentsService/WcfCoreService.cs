﻿using Common.Parents;
using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace DocumentsService
{
    public class WcfCoreService : AuthenticatingWcfService, IWcfDocumentsService
    {
        private IDocumentsService _appService;
        private IWcfUsersService _usersService;
        private IWcfFileCabinetsService _fileCabinetsService;

        public WcfCoreService(IWcfAuthenticationService authService, IDocumentsService documentsService,
            IWcfUsersService usersService, IWcfFileCabinetsService fileCabinetsService)
            : base(authService)
        {
            _appService = documentsService;
            _usersService = usersService;
            _fileCabinetsService = fileCabinetsService;
        }

        public Document GetDocumentById(string guid)
        {
            var doc = _appService.GetDocumentById(Guid.Parse(guid));
            return doc;
        }

        public IList<Document> GetDocuments(string fcGuid, SearchQuery searchQuery = null)
        {
            EnsureIsAuthorized();

            var docs = _appService.GetDocuments(Guid.Parse(fcGuid), searchQuery);
            return docs;
        }

        public IList<Document> GetDeleted(SearchQuery searchQuery = null)
        {
            EnsureIsAuthorized();

            var docs = _appService.GetDeleted(searchQuery);
            return docs;
        }

        public IList<Document> GetArchive(SearchQuery searchQuery = null)
        {
            EnsureIsAuthorized();

            var docs = _appService.GetArchive(searchQuery);
            return docs;
        }

        public int GetCount(string fcGuid, SearchQuery searchQuery = null)
        {
            EnsureIsAuthorized();

            int count = _appService.GetCount(Guid.Parse(fcGuid), searchQuery);
            return count;
        }

        public int GetDeletedCount(SearchQuery searchQuery = null)
        {
            EnsureIsAuthorized();

            int count = _appService.GetDeletedCount(searchQuery);
            return count;
        }

        public int GetArchiveCount(SearchQuery searchQuery = null)
        {
            EnsureIsAuthorized();

            int count = _appService.GetArchiveCount(searchQuery);
            return count;
        }

        public void Delete(IEnumerable<string> guids)
        {
            var parsedGuids = guids.Select(guid => Guid.Parse(guid));
            _appService.Delete(parsedGuids);
        }

        public void Update(Document doc)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            JwtPayload payload = WrapFuncInNewContext((IContextChannel)_authService, _authService.GetCurrentPayload);
            _appService.Update(doc, payload.Guid);
        }

        public void Archivate(string guid)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            _appService.Archivate(Guid.Parse(guid));
        }

        public void ArchivateFileCabinet(string fileCabinetGuid)
        {
            _appService.ArchivateFileCabinet(Guid.Parse(fileCabinetGuid));
        }

        public void RestoreArchivated(IEnumerable<string> guids)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            var parsedGuids = guids.Select(guid => Guid.Parse(guid));
            _appService.RestoreArchivated(parsedGuids);
        }

        public void RestoreAllArchivated()
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            _appService.RestoreAllArchivated();
        }

        public void RestoreDeleted(IEnumerable<string> guids)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            foreach (var guid in guids)
            {
                var doc = _appService.GetDocumentById(Guid.Parse(guid));
                if (!doc.Archived && doc.Deleted)
                {
                    var fileCabinet = WrapFuncInNewContext((IContextChannel)_fileCabinetsService,
                        _fileCabinetsService.GetFileCabinetById, doc.FileCabinetId.ToString());

                    if (fileCabinet.Deleted)
                    {
                        fileCabinet.Deleted = false;
                        WrapActionInNewContext((IContextChannel) _fileCabinetsService, _fileCabinetsService.Update, fileCabinet);
                    }
                    doc.Deleted = false;

                    JwtPayload payload = WrapFuncInNewContext((IContextChannel)_authService, _authService.GetCurrentPayload);
                    _appService.Update(doc, payload.Guid);
                }
            }
        }

        public void RestoreAllDeleted()
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            var docs = _appService.GetDeleted();
            var guids = docs.Select(doc => doc.Guid.ToString());
            RestoreDeleted(guids);
        }

        public Document Copy(Document newDocument)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            var copied = _appService.Copy(newDocument);
            return copied;
        }

        public string CompleteUpload(UploadedFileInfo fileInfo)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            var newfilePath = _appService.CompleteUpload(fileInfo);
            return newfilePath;
        }

        public Document Add(Document doc)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            JwtPayload payload = WrapFuncInNewContext((IContextChannel)_authService, _authService.GetCurrentPayload);

            doc.StoreDate = DateTime.Now;
            doc.OwnerId = payload.Guid;

            doc.Owner = WrapFuncInNewContext((IContextChannel)_usersService, _usersService.GetUserById, 
                payload.Guid.ToString());

            var addedDoc = _appService.Add(doc);
            return addedDoc;
        }

        public IList<Document> CompleteImport(UploadedFileInfo importInfo)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            try
            {
                var importedDocs = _appService.CompleteImport(importInfo);
                return importedDocs;
            }
            catch (InvalidOperationException ex)
            {
                throw new FaultException(ex.Message);
            }
        }

        public Guid GetRandomGuid()
        {
            return Guid.NewGuid();
        }

        public void UploadChunks(string guid, string fileName, byte[] chunk)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            _appService.UploadChunks(guid, fileName, chunk);
        }
    }
}