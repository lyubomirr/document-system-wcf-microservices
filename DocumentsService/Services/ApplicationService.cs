﻿using Interfaces.Repositories;
using Interfaces.Services;
using Ionic.Zip;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace DocumentsService.Services
{
    public class ApplicationService : IDocumentsService
    {
        private IDocumentsRepository _documentsRepository;

        private readonly string _uploadsDirectoryPath;
        private readonly string _tempFilesDirectoryPath;

        public ApplicationService(IDocumentsRepository documentsRepository,
            string uploadsDirectoryPath, string tempFilesDirectoryPath)
        {
            _documentsRepository = documentsRepository;
            _uploadsDirectoryPath = uploadsDirectoryPath;
            _tempFilesDirectoryPath = tempFilesDirectoryPath;
        }

        public IList<Document> GetDocuments(Guid? fcGuid = null, SearchQuery searchQuery = null)
        {   
            var documentsInFc = _documentsRepository.GetDocuments(fcGuid, searchQuery);
            return documentsInFc;
        }

        public IList<Document> GetDeleted(SearchQuery searchQuery = null)
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Deleted, searchQuery);
            return docs;
        }

        public IList<Document> GetArchive(SearchQuery searchQuery = null)
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Archived, searchQuery);
            return docs;
        }

        public int GetCount(Guid? fcGuid = null, SearchQuery searchQuery = null)
        {
            int count = _documentsRepository.GetDocumentCount(fcGuid, searchQuery);
            return count;
        }

        public int GetDeletedCount(SearchQuery searchQuery = null)
        {
            int count = _documentsRepository.GetInactiveDocumentCount(InactiveState.Deleted, searchQuery);
            return count;
        }

        public int GetArchiveCount(SearchQuery searchQuery = null)
        {
            int count = _documentsRepository.GetInactiveDocumentCount(InactiveState.Archived, searchQuery);
            return count;
        }

        public void Delete(IEnumerable<Guid> guids)
        {
            foreach (var guid in guids)
            {
                var doc = _documentsRepository.GetDocumentById(guid);

                if (!doc.Archived)
                {
                    doc.Deleted = true;
                    _documentsRepository.UpdateDocument(doc);
                }
            }
        }

        public void Update(Document doc, Guid userModifiedGuid)
        {
            doc.LastModifiedDate = DateTime.Now;
            doc.LastModifiedUser = userModifiedGuid;

            SaveDocumentMetaData(doc);
            _documentsRepository.UpdateDocument(doc);
        }

        public void Archivate(Guid guid)
        {
            var doc = _documentsRepository.GetDocumentById(guid);
            if (doc == null)
            {
                return;
            }
            doc.Archived = true;
            _documentsRepository.UpdateDocument(doc);
        }

        public void ArchivateFileCabinet(Guid fileCabinetGuid)
        {
            var docsInFc = GetDocuments(fileCabinetGuid);
            foreach (var doc in docsInFc)
            {
                Archivate(doc.Guid);
            }
        }

        public void RestoreArchivated(IEnumerable<Guid> guids)
        {
            foreach(var guid in guids)
            {
                var doc = _documentsRepository.GetDocumentById(guid);
                if(!doc.Deleted && doc.Archived)
                {
                    doc.Archived = false;
                    _documentsRepository.UpdateDocument(doc);
                }
            }
        }

        public void RestoreAllArchivated()
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Archived);
            foreach(var doc in docs)
            {
                doc.Archived = false;
                _documentsRepository.UpdateDocument(doc);
            }
        }

        public Document Copy(Document newDocument)
        {
            newDocument.CopyFromDocumentId = newDocument.Guid;
            newDocument.Guid = Guid.NewGuid();
            newDocument.StoreDate = DateTime.Now;

            string oldPath = newDocument.FilePath;
            string fileName = Path.GetFileName(oldPath);

            newDocument.FilePath = Path.Combine(
                newDocument.FileCabinetId.ToString(),
                newDocument.Guid.ToString(), 
                fileName
            );

            string directoryPath = Path.Combine(_uploadsDirectoryPath,
                newDocument.FileCabinetId.ToString(), newDocument.Guid.ToString());

            Directory.CreateDirectory(directoryPath);

            File.Copy(
                Path.Combine(_uploadsDirectoryPath, oldPath), 
                Path.Combine(_uploadsDirectoryPath, newDocument.FilePath)
            );

            SaveDocumentMetaData(newDocument);
            _documentsRepository.AddDocument(newDocument);

            return newDocument;
        }

        public void UploadChunks(string guid, string fileName, byte[] chunk)
        {
            try
            {
                var directory = Directory.CreateDirectory(Path.Combine(_tempFilesDirectoryPath, guid));
                var originalFilePath = Path.Combine(directory.FullName, fileName);
                if (!File.Exists(originalFilePath))
                {
                    using (var fs = new FileStream(originalFilePath, FileMode.Create))
                    {
                        fs.Write(chunk, 0, chunk.Length);
                    }
                    return;
                }

                AppendFile(originalFilePath, chunk);

                return;
            }
            catch (Exception)
            {
                Directory.Delete(Path.Combine(_tempFilesDirectoryPath, guid), true);
            }
        }

        public string CompleteUpload(UploadedFileInfo fileInfo)
        {

            string newFileDirectoryPath = Path.Combine(_uploadsDirectoryPath,
                fileInfo.FileCabinetGuid.ToString(), fileInfo.Guid.ToString());
               
            string newFilePath = Path.Combine(newFileDirectoryPath, fileInfo.FileName);
            string tempFilePath = Path.Combine(_tempFilesDirectoryPath, fileInfo.Guid.ToString(), fileInfo.FileName);

            Directory.CreateDirectory(newFileDirectoryPath);
            File.Copy(tempFilePath, newFilePath);
            Directory.Delete(Path.GetDirectoryName(tempFilePath), true);

            string newFileRelativePath = Path.Combine(fileInfo.FileCabinetGuid.ToString(),
                fileInfo.Guid.ToString(), fileInfo.FileName);

            return newFileRelativePath;
        }

        public Document Add(Document doc)
        {
            try
            {
                SaveDocumentMetaData(doc);
                _documentsRepository.AddDocument(doc);

                return doc;
            }
            catch (Exception)
            {
                var fileAbsPath = Path.Combine(_uploadsDirectoryPath, doc.FilePath);
                var directoryPath = Path.GetDirectoryName(fileAbsPath); 
                Directory.Delete(directoryPath, true);
                return null;
            }
        }

        public IList<Document> CompleteImport(UploadedFileInfo importInfo)
        {
            string tempFilesPath = Path.Combine(_tempFilesDirectoryPath, importInfo.Guid.ToString());
            string newFilePath = Path.Combine(tempFilesPath, importInfo.FileName);

            var importedDocs = new List<Document>();
            ZipFile zip = new ZipFile();

            try
            {
                zip = ZipFile.Read(newFilePath);
            }
            catch
            {
                Directory.Delete(Path.GetDirectoryName(newFilePath), true);
                throw new InvalidOperationException("Invalid zip file!");
            }

            var directories = GetZipDirectories(zip);
            foreach (var directory in directories)
            {
                var newDirectoryGuid = Guid.NewGuid();
                var files = zip.Where(entry => entry.FileName.StartsWith(directory)).Select(entry => entry).ToList();
                if (files.Count != 2 || !files.Any(entry => Path.GetFileName(entry.FileName) == "data.xml"))
                {
                    continue;
                }

                foreach(var entry in files)
                {
                    string onlyFileName = Path.GetFileName(entry.FileName);
                    entry.FileName = Path.Combine(newDirectoryGuid.ToString(), onlyFileName);
                    var entryPath = Path.Combine(_uploadsDirectoryPath, importInfo.FileCabinetGuid.ToString());
                    entry.Extract(entryPath);

                    if (onlyFileName == "data.xml")
                    {
                        Document deserialized;
                        try
                        {
                            deserialized = DeserializeDocument(entry.FileName, importInfo.FileCabinetGuid,
                                newDirectoryGuid);
                        }
                        catch (InvalidOperationException)
                        {
                            var documentPath = Path.Combine(entryPath, entry.FileName);
                            Directory.Delete(Path.GetDirectoryName(documentPath), true);
                            break;
                        }

                        _documentsRepository.AddDocument(deserialized);
                        var populated = _documentsRepository.GetDocumentById((Guid)deserialized.Guid);
                        importedDocs.Add(populated);
                    }

                }
            }

            zip.Dispose();

            Directory.Delete(Path.GetDirectoryName(newFilePath), true);
            return importedDocs;
        } 

        private List<string> GetZipDirectories(ZipFile zip)
        {
            List<string> directories = new List<string>();
            foreach (var entry in zip.Entries)
            {
                directories.Add(Path.GetDirectoryName(entry.FileName));
            }
            return directories.Select(dir => dir).Distinct().ToList();
        }

        private void SaveDocumentMetaData(Document doc)
        {
            var fileDirectory = Path.GetDirectoryName(doc.FilePath);
            var metaDataFilePath = Path.Combine(_uploadsDirectoryPath, fileDirectory, "data.xml");

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Document));
            using (FileStream file = new FileStream(metaDataFilePath, FileMode.Create))
            {
                xmlSerializer.Serialize(file, doc);
            }
        }

        private Document DeserializeDocument(string entryName, Guid fileCabinetGuid, 
            Guid newGuid)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Document));

            var xmlFilePath = Path.Combine(_uploadsDirectoryPath,
                fileCabinetGuid.ToString(), entryName);

            Document deserialized = new Document();
            using (FileStream fs = new FileStream(xmlFilePath, FileMode.Open))
            {
                deserialized = (Document)serializer.Deserialize(fs);
            }

            string fileName = Path.GetFileName(deserialized.FilePath);

            deserialized.Guid = newGuid;
            deserialized.FileCabinetId = fileCabinetGuid;
            deserialized.FilePath = Path.Combine(fileCabinetGuid.ToString(),
                deserialized.Guid.ToString(), fileName);
            deserialized.StoreDate = DateTime.Now;

            return deserialized;
        }

        private void AppendFile(string masterFilePath, byte[] partial)
        {
            using (FileStream master = new FileStream(masterFilePath, FileMode.Append))
            {   
                master.Write(partial, 0, partial.Length);
            }
        }

        public Document GetDocumentById(Guid guid)
        {
            return _documentsRepository.GetDocumentById(guid);
        }
    }
}
