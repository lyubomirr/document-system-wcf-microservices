﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace Configuration
{
    public static class TableMapper
    {
        public static TableMapping GetTableMapping(string tableName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TableMappingConfig));
            TableMappingConfig tableMapping;
            Assembly assembly = Assembly.GetExecutingAssembly();

            var configStream = assembly.GetManifestResourceStream("Configuration.TableMappingConfig.xml");
            tableMapping = (TableMappingConfig)serializer.Deserialize(configStream);
            return tableMapping.TableMappings.FirstOrDefault(i => i.TableName == tableName);
        }
    }
}
