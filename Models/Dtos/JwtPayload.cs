﻿using System;
using System.Runtime.Serialization;

namespace Models.Dtos
{
    [DataContract]
    public class JwtPayload
    {
        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public long exp { get; set; }

        public override string ToString()
        {
            return $"Guid: {Guid}, Role: {Role}, exp:{exp}";
        }
    }
}