﻿using System;
using System.Runtime.Serialization;

namespace Models.DbEntities
{
    [DataContract]
    public class PendingUser
    {
        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public Guid PendingUserGuid { get; set; }
        [DataMember]
        public User User { get; set; }
    }
}
