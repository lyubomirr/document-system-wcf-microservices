﻿using System;
using System.Runtime.Serialization;

namespace Models.DbEntities
{
    [DataContract]
    public class DocumentType
    {
        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}