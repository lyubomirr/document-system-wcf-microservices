﻿using System;
using System.Runtime.Serialization;

namespace Models.DbEntities
{
    [DataContract]
    public class FileCabinet
    {
        [DataMember]
        public Guid? Guid { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        public override string ToString()
        {
            return $"Guid: {Guid}, Name:{Name}, Deleted={Deleted}";
        }
    }
}