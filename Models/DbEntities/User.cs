﻿using System;
using System.Runtime.Serialization;

namespace Models.DbEntities
{
    [DataContract]
    public class User
    {
        [DataMember]
        public Guid? Guid { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Salt { get; set; }
        [DataMember]
        public Guid? RoleId { get; set; }

        public override string ToString()
        {
            return $"Guid:{Guid} Username: {Username} Password:{Password} Salt:{Salt} RoleId:{RoleId}";
        }
    }
}