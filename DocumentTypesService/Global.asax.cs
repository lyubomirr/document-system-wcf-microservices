﻿using Common.Ninject;
using DocumentTypesService.Ninject;
using Ninject;
using Ninject.Web.Common.WebHost;

namespace DocumentTypesService
{
    public class Global : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            return new StandardKernel(new Module(), new DalModule());
        }
    }
}