﻿using Common.Parents;
using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using System.Collections.Generic;


namespace DocumentTypesService
{
    public class WcfCoreService : AuthenticatingWcfService, IWcfDocumentTypesService
    {
        private IDocumentTypesService _appService;

        public WcfCoreService(IDocumentTypesService appService, IWcfAuthenticationService authService)
            : base(authService)
        {
            _appService = appService;
        }

        public virtual IList<DocumentType> GetDocumentTypes()
        {
            EnsureIsAuthorized();

            var docTypes = _appService.GetDocumentTypes();
            return docTypes;
        }
    }
}