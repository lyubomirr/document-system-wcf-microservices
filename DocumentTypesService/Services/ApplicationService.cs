﻿using Interfaces.Repositories;
using Interfaces.Services;
using Models.DbEntities;
using System.Collections.Generic;

namespace DocumentTypesService.Services
{
    public class ApplicationService : IDocumentTypesService
    {
        private IDocumentTypesRepository _documentTypesRepository;
        public ApplicationService(IDocumentTypesRepository documentTypesRepository)
        {
            _documentTypesRepository = documentTypesRepository;
        }

        public IList<DocumentType> GetDocumentTypes()
        {
            var docTypes = _documentTypesRepository.GetDocumentTypes();
            return docTypes;
        }
    }
}
