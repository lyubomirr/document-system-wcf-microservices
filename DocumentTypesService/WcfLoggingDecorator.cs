﻿using Interfaces.Wcf;
using log4net;
using Models.DbEntities;
using System;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace DocumentTypesService
{
    public class WcfLoggingDecorator : IWcfDocumentTypesService
    {
        private IWcfDocumentTypesService _wcfCore;
        private readonly ILog _logger;

        public WcfLoggingDecorator(IWcfDocumentTypesService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public IList<DocumentType> GetDocumentTypes()
        {
            _logger.Debug("GetDocumentTypes operation called!");
            try
            {
                return _wcfCore.GetDocumentTypes();
            }
            catch(WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetDocumentTypes operation!", webException);
                throw;
            }
            catch(Exception ex)
            {
                _logger.Error("Exception thrown at GetDocumentTypes operation!", ex);
                return null; 
            }
        }
    }
}