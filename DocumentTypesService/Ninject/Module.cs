﻿using Common.Factories;
using DocumentTypesService.Repositories;
using DocumentTypesService.Services;
using Interfaces.Repositories;
using Interfaces.Services;
using Interfaces.Wcf;
using Ninject.Modules;

namespace DocumentTypesService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IDocumentTypesService>().To<ApplicationService>();
            Bind<IDocumentTypesRepository>().To<DocumentTypesRepository>()
               .WithConstructorArgument("tableName", "DocumentTypes");

            var channelFactory = new WcfServiceChannelFactory();
            Bind<IWcfAuthenticationService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfAuthenticationService>("AuthEndpoint"));

            Bind<IWcfDocumentTypesService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>();
        }
    }
}