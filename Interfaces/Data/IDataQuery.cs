﻿using Configuration;
using System;

namespace Interfaces.Data
{
    public interface IDataQuery
    {
        IDataQuery Init(string tableName);
        IDataQuery Init(TableMapping tableMapping);
        IDataQuery AddQuery(string property, Guid? value);
        IDataQuery AddQuery(string property, string value);
        IDataQuery AddQuery(string property, object value);
        IDataQuery SortBy(string sortProperty, bool? isAsc = true);
        IDataQuery GetPage(int? pageNumber, int? elemPerPage = null);
        IDataQuery SearchBy(string property, string value);
        IDataQuery SearchBy(string[] properties, string value);
        IDataQuery Join(string joinTable, string foreignProperty, string primaryProperty);
        TableMapping GetMapping();
        TableMapping GetJoinedMapping();
        string ToSql();
    }
}
