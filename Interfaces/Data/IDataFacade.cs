﻿using System.Collections.Generic;

namespace Interfaces.Data
{
    public interface IDataFacade
    {
        IList<ModelT> GetTableData<ModelT>(IDataQuery query) where ModelT : class, new();

        IList<ModelT> GetJoinedTableData<ModelT, JoinT>(IDataQuery query, string populatedProperty) 
            where ModelT : class, new() where JoinT : class, new();

        bool DeleteObject(IDataQuery query);
        bool ModifyObject<ModelT>(ModelT modified, IDataQuery query) where ModelT: class;
        bool AddObject<ModelT>(ModelT newObject, IDataQuery query) where ModelT : class;
        int GetTableRowCount(IDataQuery query);
    }
}
