﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;

namespace Interfaces.Repositories
{
    public interface IPendingUsersRepository
    {
        bool Add(PendingUser pendingUser);
        IList<PendingUser> GetAll(SearchQuery searchQuery = null);
        PendingUser GetByUserId(Guid id);
        PendingUser GetByUsername(string username);
        void Delete(Guid id);
    }
}
