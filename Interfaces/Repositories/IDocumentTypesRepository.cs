﻿using Interfaces.Data;
using Models.DbEntities;
using System.Collections.Generic;

namespace Interfaces.Repositories
{
    public interface IDocumentTypesRepository
    {
        IList<DocumentType> GetDocumentTypes();
    }
}
