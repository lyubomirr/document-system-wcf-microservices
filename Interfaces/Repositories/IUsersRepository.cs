﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;


namespace Interfaces.Repositories
{
    public interface IUsersRepository
    {
        IList<User> GetUsers(string username = null);
        User GetUserById(Guid guid);
        void AddUser(User newUser);
        int GetCount(SearchQuery searchQuery = null, bool confirmed = true);
        User Update(User updated);
        void Delete(User user);
        void Delete(Guid guid);
    }
}
