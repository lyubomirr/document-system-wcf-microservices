﻿using Interfaces.Data;
using Models.DbEntities;
using System;
using System.Collections.Generic;


namespace Interfaces.Repositories
{
    public interface IRolesRepository
    {
        IList<Role> GetRoles();
        Role GetRoleById(Guid guid);
    }
}
