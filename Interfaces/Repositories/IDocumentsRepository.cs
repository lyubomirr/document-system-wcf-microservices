﻿using System;
using System.Collections.Generic;
using Models.DbEntities;
using Models.Dtos;

namespace Interfaces.Repositories
{
    public enum InactiveState
    {
        Archived,
        Deleted
    };

    public interface IDocumentsRepository
    {
        IList<Document> GetDocuments(Guid? fcGuid = null, SearchQuery searchQuery = null);
        IList<Document> GetInactiveDocuments(InactiveState state, SearchQuery searchQuery = null, Guid? fcGuid = null);
        Document GetDocumentById(Guid guid);
        Document UpdateDocument(Document updatedDoc);     
        int GetDocumentCount(Guid? fcGuid = null, SearchQuery searchQuery = null);
        int GetInactiveDocumentCount(InactiveState state, SearchQuery searchQuery = null);
        bool AddDocument(Document doc);
    }
}
