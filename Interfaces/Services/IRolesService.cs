﻿using Models.DbEntities;
using System;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IRolesService
    {
        IList<Role> GetRoles();
        Role GetRoleById(Guid guid);
    }
}
