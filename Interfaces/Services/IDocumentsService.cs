﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;

namespace Interfaces.Services
{
    public interface IDocumentsService
    {
        Document GetDocumentById(Guid guid);
        IList<Document> GetDocuments(Guid? fcGuid = null, SearchQuery searchQuery = null);
        IList<Document> GetDeleted(SearchQuery searchQuery = null);
        IList<Document> GetArchive(SearchQuery searchQuery = null);
        IList<Document> CompleteImport(UploadedFileInfo importInfo);
        Document Copy(Document newDocument);
        Document Add(Document doc);
        int GetCount(Guid? fcGuid = null, SearchQuery searchQuery = null);
        int GetDeletedCount(SearchQuery searchQuery = null);
        int GetArchiveCount(SearchQuery searchQuery = null);
        void UploadChunks(string guid, string fileName, byte[] chunk);
        string CompleteUpload(UploadedFileInfo fileInfo);
        void Delete(IEnumerable<Guid> guids);
        void Update(Document doc, Guid userModifiedGuid);
        void Archivate(Guid guid);
        void ArchivateFileCabinet(Guid fileCabinetGuid);
        void RestoreArchivated(IEnumerable<Guid> guids);
        void RestoreAllArchivated();
    }
}
