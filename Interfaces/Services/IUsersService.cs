﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IUsersService
    {
        IList<User> GetUsers(string username = null);
        IList<User> GetUnconfimedUsers(SearchQuery searchQuery = null);
        User GetUserById(Guid guid);
        User Login(User loggingUser);
        void Register(User registeringUser);
        int GetCount(SearchQuery searchQuery = null, bool confirmed = true);
        void ApproveUserRequest(Guid userGuid, Role assignedRole);
        void RejectUserRequest(Guid userGuid);
    }
}
