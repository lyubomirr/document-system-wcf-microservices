﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfDocumentsService
    {
        [OperationContract]
        Document GetDocumentById(string guid);

        [OperationContract]
        IList<Document> GetDocuments(string fcGuid, SearchQuery searchQuery = null);

        [OperationContract]
        IList<Document> GetDeleted(SearchQuery searchQuery = null);

        [OperationContract]
        IList<Document> GetArchive(SearchQuery searchQuery = null);

        [OperationContract]
        int GetCount(string fcGuid, SearchQuery searchQuery = null);

        [OperationContract]
        int GetDeletedCount(SearchQuery searchQuery = null);

        [OperationContract]
        int GetArchiveCount(SearchQuery searchQuery = null);

        [OperationContract]
        void Delete(IEnumerable<string> guids);

        [OperationContract]
        void Update(Document doc);

        [OperationContract]
        void Archivate(string guid);

        [OperationContract]
        void ArchivateFileCabinet(string fileCabinetGuid);

        [OperationContract]
        void RestoreArchivated(IEnumerable<string> guids);

        [OperationContract]
        void RestoreAllArchivated();

        [OperationContract]
        void RestoreDeleted(IEnumerable<string> guids);

        [OperationContract]
        void RestoreAllDeleted();
        [OperationContract]
        Document Copy(Document newDocument);

        [OperationContract]
        string CompleteUpload(UploadedFileInfo fileInfo);

        [OperationContract]
        Document Add(Document doc);

        [OperationContract]
        IList<Document> CompleteImport(UploadedFileInfo importInfo);

        [OperationContract]
        Guid GetRandomGuid();
        
        [OperationContract]
        void UploadChunks(string guid, string fileName, byte[] chunk);
    }
}
