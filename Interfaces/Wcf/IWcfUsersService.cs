﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfUsersService
    {
        [OperationContract]
        User GetCurrentUser();
        [OperationContract]
        User GetUserById(string guid);
        [OperationContract]
        User Login(User loggedUser);
        [OperationContract]
        void Logout();
        [OperationContract]
        void Register(User registeringUser);
        [OperationContract]
        IList<User> GetUnconfirmedUsers(SearchQuery searchQuery = null);
        [OperationContract]
        int GetUnconfirmedCount(SearchQuery searchQuery = null);
        [OperationContract]
        void ApproveUser(Guid userGuid, string roleName);
        [OperationContract]
        void RejectUser(Guid userGuid);
    }
}
