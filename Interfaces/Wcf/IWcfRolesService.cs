﻿using Models.DbEntities;
using System;
using System.Collections.Generic;
using System.ServiceModel;


namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfRolesService
    {
        [OperationContract]
        IList<Role> GetRoles();
        [OperationContract]
        Role GetRoleById(Guid guid);
    }
}
