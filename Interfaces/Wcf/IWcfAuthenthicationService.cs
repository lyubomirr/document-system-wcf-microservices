﻿using Models.Dtos;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfAuthenticationService
    {
        [OperationContract]
        bool IsAuthorized();
        [OperationContract]
        bool IsGuest();
        [OperationContract]
        bool IsAdmin();
        [OperationContract]
        string CreateToken(JwtPayload payload);
        [OperationContract]
        JwtPayload GetCurrentPayload();
    }

}
