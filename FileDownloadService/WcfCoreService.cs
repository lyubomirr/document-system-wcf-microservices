﻿using Common.Parents;
using FileDownloadService.Helpers;
using Interfaces.Wcf;
using Ionic.Zip;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace FileDownloadService
{
    public class WcfCoreService : AuthenticatingWcfService, IWcfFileService
    {
        private IWcfDocumentsService _documentsService;
        private string _tempFilesDirectoryPath;
        private string _uploadsDirectoryPath;

        public WcfCoreService(IWcfAuthenticationService authService, IWcfDocumentsService documentsService,
            string tempFilesDirectoryPath, string uploadsDirectoryPath) : base(authService)
        {
            _documentsService = documentsService;
            _tempFilesDirectoryPath = tempFilesDirectoryPath;
            _uploadsDirectoryPath = uploadsDirectoryPath;
        }

        public Stream ExportDocuments(string filename, string guids)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            var parsedGuids = guids.Split(',');
            Stream stream;

            using (ZipFile zip = new ZipFile())
            {
                foreach (var guid in parsedGuids)
                {

                    var doc = WrapFuncInNewContext((IContextChannel)_documentsService, 
                        _documentsService.GetDocumentById, guid);

                    var fileDirectory = Path.GetDirectoryName(doc.FilePath);
                    var dataFilePath = Path.Combine(_uploadsDirectoryPath, fileDirectory, "data.xml");

                    if (File.Exists(dataFilePath))
                    {
                        zip.AddFile(dataFilePath, doc.Guid.ToString());
                    }
                    if (File.Exists(Path.Combine(_uploadsDirectoryPath, doc.FilePath)))
                    {
                        zip.AddFile(Path.Combine(_uploadsDirectoryPath, doc.FilePath), doc.Guid.ToString());
                    }
                }

                stream = new DeleteOnDisposeFileStream(
                    Path.Combine(_tempFilesDirectoryPath, Path.GetRandomFileName()),
                    FileMode.Create);

                zip.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition",
                $"attachment; filename=\"{filename}.zip\"");
            return stream;
        }

        public Stream ExportFileCabinet(string filename, string guid)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            IList<Document> docsInFc = WrapFuncInNewContext((IContextChannel)_documentsService,
                _documentsService.GetDocuments, guid, (SearchQuery) null);

            if (docsInFc.Count == 0)
            {
                return null;
            }

            var guids = docsInFc.Select(doc => doc.Guid.ToString());
            var file = ExportDocuments(filename, String.Join(",", guids));

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition",
                $"attachment; filename=\"{filename}.zip\"");
            return file;
        }

        public Stream GetFile(string guid, bool isFileView = false)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            Document doc = WrapFuncInNewContext((IContextChannel)_documentsService,
                _documentsService.GetDocumentById, guid);

            if (doc == null)
            {
                return null;
            }

            var fullPath = Path.Combine(_uploadsDirectoryPath, doc.FilePath);
            var fileStream = new FileStream(fullPath, FileMode.Open);
            var fileName = Path.GetFileName(fileStream.Name);
            WebOperationContext.Current.OutgoingResponse.ContentType = MimeTypes.GetMimeType(fileName);

            if (isFileView)
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition",
                    $"inline; filename=\"{fileName}\"");
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition",
                    $"attachment; filename=\"{fileName}\"");
            }

            return fileStream;
        }
    }
}