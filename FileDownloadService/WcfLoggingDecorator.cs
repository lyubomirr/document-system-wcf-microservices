﻿using Interfaces.Wcf;
using log4net;
using System;
using System.IO;
using System.ServiceModel.Web;

namespace FileDownloadService
{
    public class WcfLoggingDecorator : IWcfFileService
    {
        private IWcfFileService _wcfCore;
        private ILog _logger;

        public WcfLoggingDecorator(IWcfFileService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public Stream ExportDocuments(string filename, string guids)
        {
            _logger.Debug($"ExportDocuments operation called with parameters filename: \"{filename}\", guids: \"{guids}\".");
            try
            {
                return _wcfCore.ExportDocuments(filename, guids);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at ExportDocuments operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at ExportDocuments operation!", ex);
                return null;
            }
        }

        public Stream ExportFileCabinet(string filename, string guid)
        {
            _logger.Debug($"ExportFileCabinet operation called with parameters filename: \"{filename}\", guid: \"{guid}\".");
            try
            {
                return _wcfCore.ExportFileCabinet(filename, guid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at ExportFileCabinet operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at ExportFileCabinet operation!", ex);
                return null;
            }
        }

        public Stream GetFile(string guid, bool isFileView = false)
        {
            _logger.Debug($"GetFile operation called with parameters guid: \"{guid}\", isFileView: \"{isFileView}\".");
            try
            {
                return _wcfCore.GetFile(guid, isFileView);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetFile operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetFile operation!", ex);
                return null;
            }
        }
    }
}