﻿using Common.Factories;
using Interfaces.Wcf;
using Ninject.Modules;
using System.Configuration;

namespace FileDownloadService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            var channelFactory = new WcfServiceChannelFactory();

            Bind<IWcfDocumentsService>()
               .ToMethod(ctx => channelFactory.CreateChannel<IWcfDocumentsService>("DocumentsServiceEndpoint"));
            Bind<IWcfAuthenticationService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfAuthenticationService>("AuthEndpoint"));

            var uploadsPath = ConfigurationManager.AppSettings["uploadsPath"];
            var tempPath = ConfigurationManager.AppSettings["tempPath"];

            Bind<IWcfFileService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>()
                .WithConstructorArgument("tempFilesDirectoryPath", tempPath)
                .WithConstructorArgument("uploadsDirectoryPath", uploadsPath);

            
        }
    }
}