﻿using Common.Factories;
using Interfaces.Repositories;
using Interfaces.Services;
using Interfaces.Wcf;
using Ninject.Modules;
using UsersService.Repositories;
using UsersService.Services;

namespace UsersService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IUsersRepository>().To<UsersRepository>()
               .WithConstructorArgument("tableName", "Users");
            Bind<IPendingUsersRepository>().To<PendingUsersRepository>()
                .WithConstructorArgument("tableName", "PendingUsers");
            
            Bind<IUsersService>().To<ApplicationService>();

            Bind<IWcfUsersService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>();

            var channelFactory = new WcfServiceChannelFactory();
            Bind<IWcfAuthenticationService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfAuthenticationService>("AuthEndpoint"));
            Bind<IWcfRolesService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfRolesService>("RolesServiceEndpoint"));
        }
    }
}