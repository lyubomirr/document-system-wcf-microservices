﻿using System;
using System.Collections.Generic;
using Common.Parents;
using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Models.Dtos;

namespace UsersService.Repositories
{
    public class PendingUsersRepository : DbRepository, IPendingUsersRepository
    {
        public PendingUsersRepository(IDataFacade dataFacade, IDataQuery dq, string tableName) 
            : base(dataFacade, dq, tableName) { }

        public bool Add(PendingUser pendingUser)
        {
            _dq.Init(_tableName);
            return _dataFacade.AddObject<PendingUser>(pendingUser, _dq);
        }

        public IList<PendingUser> GetAll(SearchQuery searchQuery = null)
        {
            _dq.Init(_tableName)
                .Join("Users", "PendingUserGuid", "Guid");

            if(searchQuery != null)
            {
                _dq.GetPage(searchQuery.PageNumber, searchQuery.ElementsPerPage)
                    .SortBy("Username", searchQuery.IsAscending);
            }

            var pendingUsers = _dataFacade.GetJoinedTableData<PendingUser, User>(_dq, "User");
            return pendingUsers;
        }

        public PendingUser GetByUserId(Guid id)
        {
            _dq.Init(_tableName)
                .Join("Users", "PendingUserGuid", "Guid")
                .AddQuery("PendingUserGuid", id);

            var result = _dataFacade.GetJoinedTableData<PendingUser, User>(_dq, "User");
            if (result.Count == 0)
            {
                return null;
            }
            return result[0];
        }

        public PendingUser GetByUsername(string username)
        {
            _dq.Init(_tableName)
               .Join("Users", "PendingUserGuid", "Guid")
               .AddQuery("Username", username);

            var result = _dataFacade.GetJoinedTableData<PendingUser, User>(_dq, "User");
            if(result.Count == 0)
            {
                return null;
            }
            return result[0];
        }

        public void Delete(Guid id)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", id);
            
            if(!_dataFacade.DeleteObject(_dq))
            {
                throw new InvalidOperationException("Pending user wasn't removed!");
            }
        }
    }
}