﻿using Common.Parents;
using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;

namespace UsersService.Repositories
{
    public class UsersRepository : DbRepository, IUsersRepository
    {
        public UsersRepository(IDataFacade dataFacade, IDataQuery dq, string tableName)
            : base(dataFacade, dq, tableName) { }

        public IList<User> GetUsers(string username = null)
        {
            _dq.Init(_tableName)
                .AddQuery("Username", username);

            return _dataFacade.GetTableData<User>(_dq);
        }

        public User GetUserById(Guid guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);

            var users = _dataFacade.GetTableData<User>(_dq);

            if (users.Count == 0)
            {
                return null;
            }

            return users[0];
        }

        public void AddUser(User newUser)
        {
            _dq.Init(_tableName);
            if(!_dataFacade.AddObject<User>(newUser, _dq))
            {
                throw new InvalidOperationException("Couldn't add user!");
            }
        }

        public int GetCount(SearchQuery searchQuery = null, bool confirmed = true)
        {
            _dq.Init(_tableName);

            int isConfirmed = confirmed ? 1 : 0;
            _dq.AddQuery("Confirmed", isConfirmed);

            if(searchQuery != null)
            {
                _dq.SearchBy("Username", searchQuery.SearchValue);
            }

            return _dataFacade.GetTableRowCount(_dq);
        }

        public User Update(User updated)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", updated.Guid);

            if(!_dataFacade.ModifyObject<User>(updated, _dq))
            {
                throw new InvalidOperationException("User wasn't updated!");
            }

            return updated;
        }

        public void Delete(User user)
        {
            _dq.Init(_tableName).AddQuery("Guid", user.Guid);
            _dataFacade.DeleteObject(_dq);
        }

        public void Delete(Guid guid)
        {
            _dq.Init(_tableName).AddQuery("Guid", guid);
            _dataFacade.DeleteObject(_dq);
        }
    }
}