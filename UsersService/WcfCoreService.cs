﻿using Common.Parents;
using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace UsersService
{
    public class WcfCoreService : AuthenticatingWcfService, IWcfUsersService
    {
        private IUsersService _appService;
        private IWcfRolesService _rolesService;

        public WcfCoreService(IUsersService appService, IWcfAuthenticationService authService,
            IWcfRolesService rolesService) : base(authService)
        {
            _appService = appService;
            _rolesService = rolesService;
        }

        public void ApproveUser(Guid userGuid, string roleName)
        {
            EnsureIsAuthorized();
            EnsureIsAdmin();

            var roles = WrapFuncInNewContext((IContextChannel)_rolesService, _rolesService.GetRoles);
            var assignedRole = roles.Select(role => role).Where(role => role.Name == roleName).FirstOrDefault();
            if(assignedRole == null)
            {
                throw new ArgumentException("No such role!");
            }

            _appService.ApproveUserRequest(userGuid, assignedRole);
        }

        public User GetCurrentUser()
        {
            EnsureIsAuthorized();

            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            JwtPayload payload = WrapFuncInNewContext((IContextChannel)_authService, _authService.GetCurrentPayload);
            return _appService.GetUserById(payload.Guid);
        }

        public int GetUnconfirmedCount(SearchQuery searchQuery =  null)
        {
            EnsureIsAuthorized();
            EnsureIsAdmin();

            return _appService.GetCount(searchQuery, false);
        }

        public IList<User> GetUnconfirmedUsers(SearchQuery searchQuery)
        {
            EnsureIsAuthorized();
            EnsureIsAdmin();

            var users = _appService.GetUnconfimedUsers(searchQuery);
            return users;
        }

        public User GetUserById(string guid)
        {
            EnsureIsAuthorized();

            var user = _appService.GetUserById(Guid.Parse(guid));
            return user;
        }

        public User Login(User loggingUser)
        {
            try
            {
                var user = _appService.Login(loggingUser);
                SaveCredentials(user, HttpContext.Current.Response);
                return user;
            }
            catch (AuthenticationException ex)
            {
                throw new FaultException(ex.Message);
            }
        }

        public void Logout()
        {
            HttpCookie cookie = new HttpCookie("jwt")
            {
                Expires = DateTime.Now.AddDays(-1d)
            };
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public void Register(User registeringUser)
        {
            try
            {
                _appService.Register(registeringUser);
            }
            catch(AuthenticationException ex)
            {
                throw new FaultException(ex.Message);
            }
        }

        public void RejectUser(Guid userGuid)
        {
            EnsureIsAuthorized();
            EnsureIsAdmin();

            _appService.RejectUserRequest(userGuid);
        }

        private void SaveCredentials(User user, HttpResponse response)
        {
            Role role = WrapFuncInNewContext((IContextChannel)_rolesService, _rolesService.GetRoleById, 
                (Guid)user.RoleId);

            JwtPayload payload = new JwtPayload();
            payload.Guid = (Guid)user.Guid;
            payload.Role = role.Name;
            payload.exp = ((DateTimeOffset)(DateTime.Now.AddHours(8))).ToUnixTimeSeconds();

            string token = WrapFuncInNewContext((IContextChannel)_authService, _authService.CreateToken, payload);
            HttpCookie cookie = new HttpCookie("jwt", token);
            cookie.Expires = DateTime.Now.AddHours(8);
            response.Cookies.Add(cookie);
        }
    }
}