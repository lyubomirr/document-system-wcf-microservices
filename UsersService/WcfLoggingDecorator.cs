﻿using Interfaces.Wcf;
using log4net;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace UsersService
{
    public class WcfLoggingDecorator : IWcfUsersService
    {
        private IWcfUsersService _wcfCore;
        private readonly ILog _logger;

        public WcfLoggingDecorator(IWcfUsersService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public User GetCurrentUser()
        {
            _logger.Debug("GetCurrentUser operation called!");
            try
            {
                return _wcfCore.GetCurrentUser();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetCurrentUser operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetCurrentUser operation!", ex);
                return null;
            }
        }

        public User GetUserById(string guid)
        {
            _logger.Debug($"GetUserById operation called with parameter guid: \"{guid}\"");
            try
            {
                return _wcfCore.GetUserById(guid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetUserById operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetUserById operation!", ex);
                return null;
            }
        }

        public User Login(User loggedUser)
        {
            _logger.Debug($"Login operation called with username={loggedUser.Username}!");
            try
            {
                return _wcfCore.Login(loggedUser);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Login operation!", webException);
                throw;
            }
            catch (FaultException faultException)
            {
                _logger.Error("FaultException thrown at Login operation!", faultException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Login operation!", ex);
                return null;
            }
        }

        public void Logout()
        {
            _logger.Debug("Logout operation called!");
            try
            {
                _wcfCore.Logout();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Logout operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Logout operation!", ex);
            }
        }

        public void Register(User registeringUser)
        {
            _logger.Debug($"Register operation called with username={registeringUser.Username}!");
            try
            {
                _wcfCore.Register(registeringUser);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Register operation!", webException);
                throw;
            }
            catch (FaultException faultException)
            {
                _logger.Error("FaultException thrown at Register operation!", faultException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Register operation!", ex);
            }
        }

        public IList<User> GetUnconfirmedUsers(SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetUnconfirmedUsers operation called with parameter searchQuery: {{{searchQuery}}}!");
            try
            {
                return _wcfCore.GetUnconfirmedUsers(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetUnconfirmedUsers operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetUnconfirmedUsers operation!", ex);
                return null;
            }
        }
        public int GetUnconfirmedCount(SearchQuery searchQuery = null)
        {
            _logger.Debug($"GetUnconfirmedCount operation called with parameter searchQuery: {{{searchQuery}}}!");
            try
            {
                return _wcfCore.GetUnconfirmedCount(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetUnconfirmedCount operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetUnconfirmedCount operation!", ex);
                return 0;
            }
        }

        public void ApproveUser(Guid userGuid, string roleName)
        {
            _logger.Debug($"ApproveUser operation called with parameter userGuid=\"{userGuid}\"!");
            try
            {
                _wcfCore.ApproveUser(userGuid, roleName);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at ApproveUser operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at ApproveUser operation!", ex);
            }
        }

        public void RejectUser(Guid userGuid)
        {
            _logger.Debug($"RejectUser operation called with parameter userGuid=\"{userGuid}\"!");
            try
            {
                _wcfCore.RejectUser(userGuid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at RejectUser operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at RejectUser operation!", ex);
            }
        }
    }
}