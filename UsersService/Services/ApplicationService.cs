﻿using Interfaces.Repositories;
using Interfaces.Services;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Security.Authentication;
using System.Threading.Tasks;
using UsersService.Helpers;

namespace UsersService.Services
{
    public class ApplicationService : IUsersService
    {
        private IUsersRepository _usersRepository;
        private IPendingUsersRepository _pendingUsersRepository;

        public ApplicationService(IUsersRepository usersRepository, IPendingUsersRepository pendingUsersRepository)
        {
            _usersRepository = usersRepository;
            _pendingUsersRepository = pendingUsersRepository;
        }

        public IList<User> GetUnconfimedUsers(SearchQuery searchQuery = null)
        {
            var users = _pendingUsersRepository.GetAll(searchQuery).Select(x => x.User).ToList();
            return users;
        }

        public int GetCount(SearchQuery searchQuery, bool confirmed = true)
        {
            return _usersRepository.GetCount(searchQuery, confirmed);
        }

        public User GetUserById(Guid guid)
        {
            var user = _usersRepository.GetUserById(guid);
            return user;
        }

        public IList<User> GetUsers(string username = null)
        {
            var users = _usersRepository.GetUsers(username);
            return users;
        }

        public User Login(User loggingUser)
        {
            Hashing hashing = new Hashing();

            var pendingUser = _pendingUsersRepository.GetByUsername(loggingUser.Username);
            if(pendingUser != null)
            {
                throw new AuthenticationException("NotConfirmed");
            }

            var users = GetUsers(loggingUser.Username);

            if (users.Count == 0)
            {
                throw new AuthenticationException("NoUser");
            }

            if (!hashing.VerifyHash(loggingUser.Password, users[0].Password, users[0].Salt))
            {
                throw new AuthenticationException("WrongPassword");
            }
            
            return users[0];
        }

        public void Register(User registeringUser)
        {
            Hashing hashing = new Hashing();

            var users = GetUsers(registeringUser.Username);
            if(users.Count != 0)
            {
                throw new AuthenticationException("UserRegistered");
            }

            registeringUser.Guid = Guid.NewGuid();
            registeringUser.Salt = hashing.GenerateSalt();
            registeringUser.Password = hashing.ComputeHash(registeringUser.Password, registeringUser.Salt);

            var pending = new PendingUser { Guid = Guid.NewGuid(), PendingUserGuid = (Guid)registeringUser.Guid };

            _usersRepository.AddUser(registeringUser);
            _pendingUsersRepository.Add(pending);

            Task.Run(() => SendEmail(registeringUser.Username));
        }

        public void ApproveUserRequest(Guid userGuid, Role assignedRole)
        {
            var pendingUserRequest = _pendingUsersRepository.GetByUserId(userGuid);
            if (pendingUserRequest == null)
            {
                throw new ArgumentException("No pending user with such guid!");
            }

            var user = _usersRepository.GetUserById(pendingUserRequest.PendingUserGuid);

            if(user == null)
            {
                throw new InvalidOperationException("No user with id as the pending user key!");
            }

            user.RoleId = assignedRole.Guid;
            _usersRepository.Update(user);
            _pendingUsersRepository.Delete(pendingUserRequest.Guid);
        }

        public void RejectUserRequest(Guid userGuid)
        {
            var pendingUserRequest = _pendingUsersRepository.GetByUserId(userGuid);
            if (pendingUserRequest == null)
            {
                throw new ArgumentException("No user with such guid!");
            }

            _pendingUsersRepository.Delete(pendingUserRequest.Guid);
            _usersRepository.Delete(userGuid); 
        }

        private void SendEmail(string username)
        {
            string email = ConfigurationManager.AppSettings["adminEmail"];
            string password = ConfigurationManager.AppSettings["adminEmailPassword"];
            string adminPanelLink = ConfigurationManager.AppSettings["adminPanelLink"];
            string smtpSever = ConfigurationManager.AppSettings["smtpServer"];
            int smtpPort = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);

            string mailBody = $"New user registered with username: {username}. Click here to process it: {adminPanelLink} .";

            var msg = new MailMessage(email, email, "New Registration", mailBody);

            var smtpClient = new SmtpClient(smtpSever, smtpPort);
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(email, password);
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
        }
    }
}