﻿using Interfaces.Wcf;
using log4net;
using Models.DbEntities;
using System;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace RolesService
{
    public class WcfLoggingDecorator : IWcfRolesService
    {
        private IWcfRolesService _wcfCore;
        private readonly ILog _logger;

        public WcfLoggingDecorator(IWcfRolesService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public Role GetRoleById(Guid guid)
        {
            _logger.Debug($"GetRoleById operation called with argument guid: \"{guid}\".");
            try
            {
                return _wcfCore.GetRoleById(guid);
            }
            catch(WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetRoleById operation!", webException);
                throw;
            }
            catch(Exception ex)
            {
                _logger.Error("Exception thrown at GetRoleById operation!", ex);
                return null;
            }
        }

        public IList<Role> GetRoles()
        {
            _logger.Debug("GetRoles operation called!");
            try
            {
                return _wcfCore.GetRoles();
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetRoles operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetRoles operation!", ex);
                return null;
            }
        }
    }
}