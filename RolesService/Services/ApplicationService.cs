﻿using System.Collections.Generic;
using Interfaces.Services;
using Interfaces.Repositories;
using Models.DbEntities;
using System;

namespace RolesService.Services
{
    public class ApplicationService : IRolesService
    {
        private IRolesRepository _rolesRepository;

        public ApplicationService(IRolesRepository rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        public IList<Role> GetRoles()
        {
            var roles = _rolesRepository.GetRoles();
            return roles;
        }

        public Role GetRoleById(Guid guid)
        {
            var role = _rolesRepository.GetRoleById(guid);
            return role;
        }
    }
}
