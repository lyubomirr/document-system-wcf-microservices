﻿using Common.Factories;
using Interfaces.Repositories;
using Interfaces.Services;
using Interfaces.Wcf;
using Ninject.Modules;
using RolesService.Repositories;
using RolesService.Services;

namespace RolesService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IRolesService>().To<ApplicationService>();
            Bind<IRolesRepository>().To<RolesRepository>()
               .WithConstructorArgument("tableName", "Roles");

            var channelFactory = new WcfServiceChannelFactory();
            Bind<IWcfAuthenticationService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfAuthenticationService>("AuthEndpoint"));

            Bind<IWcfRolesService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>();
        }
    }
}