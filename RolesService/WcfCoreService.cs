﻿using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using System;
using System.Collections.Generic;

namespace RolesService
{
    public class WcfCoreService : IWcfRolesService
    {
        private IRolesService _appService;

        public WcfCoreService(IRolesService appService)
        {
            _appService = appService;
        }

        public Role GetRoleById(Guid guid)
        {
            return _appService.GetRoleById(guid);
        }

        public IList<Role> GetRoles()
        {
            var roles = _appService.GetRoles();
            return roles;
        }
    }
}