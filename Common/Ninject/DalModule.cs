﻿using Common.Factories;
using DataAccessLayer;
using Interfaces.Data;
using Ninject.Modules;
using System.Configuration;

namespace Common.Ninject
{
    public class DalModule : NinjectModule
    {
        public override void Load()
        {
            var databaseType = ConfigurationManager.AppSettings["databaseType"];
            var dataAccessFactory = new DataAccessFactory();

            Bind<IDataAdapter>().ToMethod(context => dataAccessFactory.CreateDataAdapter(databaseType));
            Bind<IDataQuery>().ToMethod(context => dataAccessFactory.CreateDataQuery(databaseType));
            Bind<IDataFacade>().To<DatabaseDataFacade>();
        }        
    }
}