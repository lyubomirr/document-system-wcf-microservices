﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Common.Parents
{
    public class WcfContextWrapper
    {
        protected TResult WrapFuncInNewContext<TResult>(IContextChannel channel, Func<TResult> func)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                return func();
            }
        }

        protected TResult WrapFuncInNewContext<TResult, T1>(IContextChannel channel, Func<T1, TResult> func, T1 param)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                return func(param);
            }
        }

        protected TResult WrapFuncInNewContext<TResult, T1, T2>(IContextChannel channel, Func<T1,T2, TResult> func, 
            T1 firstParam, T2 secondParam)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                return func(firstParam, secondParam);
            }
        }

        protected TResult WrapFuncInNewContext<TResult, T1, T2, T3>(IContextChannel channel, Func<T1, T2, T3, TResult> func,
            T1 firstParam, T2 secondParam, T3 thirdParam)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                return func(firstParam, secondParam, thirdParam);
            }
        }

        protected void WrapActionInNewContext(IContextChannel channel, Action action)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                action();
            }
        }

        protected void WrapActionInNewContext<T>(IContextChannel channel, Action<T> action, T param)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                action(param);
            }
        }

        protected void WrapActionInNewContext<T1, T2>(IContextChannel channel, Action<T1, T2> action, T1 firstParam, T2 secondParam)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                action(firstParam, secondParam);
            }
        }

        protected void WrapActionInNewContext<T1, T2, T3>(IContextChannel channel, Action<T1, T2, T3> action,
            T1 firstParam, T2 secondParam, T3 thirdParam)
        {
            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];
            using (new OperationContextScope(channel))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                action(firstParam, secondParam, thirdParam);
            }
        }
    }
}
