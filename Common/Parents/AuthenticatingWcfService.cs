﻿using Interfaces.Wcf;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Common.Parents
{
    public class AuthenticatingWcfService : WcfContextWrapper
    {
        protected IWcfAuthenticationService _authService;

        protected AuthenticatingWcfService(IWcfAuthenticationService authService)
        {
            _authService = authService;
        }

        protected void EnsureIsAuthorized()
        {
            bool isAuthorized = WrapFuncInNewContext((IContextChannel)_authService, _authService.IsAuthorized);
            if(isAuthorized)
            {
                return;
            }

            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
            throw new WebFaultException(HttpStatusCode.Unauthorized);
        }

        protected void EnsureIsAdmin()
        {
            bool isAdmin = WrapFuncInNewContext((IContextChannel)_authService, _authService.IsAdmin);
            if (isAdmin)
            {
                return;
            }

            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
            throw new WebFaultException(HttpStatusCode.Unauthorized);
        }

        protected void EnsureIsNotGuest()
        {
            bool isGuest = WrapFuncInNewContext((IContextChannel)_authService, _authService.IsGuest);
            if(!isGuest)
            {
                return;
            }

            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
            throw new WebFaultException(HttpStatusCode.Unauthorized);
        }
    }
}