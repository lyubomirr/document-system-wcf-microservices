﻿using Interfaces.Data;

namespace Common.Parents
{
    public class DbRepository
    {
        protected IDataFacade _dataFacade;
        protected IDataQuery _dq;
        protected string _tableName;

        protected DbRepository(IDataFacade dataFacade, IDataQuery dq, string tableName)
        {
            _dataFacade = dataFacade;
            _dq = dq;
            _tableName = tableName;
        }

    }
}