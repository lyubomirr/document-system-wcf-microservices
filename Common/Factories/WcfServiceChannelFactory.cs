﻿using System.ServiceModel;

namespace Common.Factories
{
    public class WcfServiceChannelFactory
    {
        public ServiceType CreateChannel<ServiceType>(string endpointName)
        {
            var channelFactory = new ChannelFactory<ServiceType>(endpointName);
            var channel = channelFactory.CreateChannel();
            return channel;
        }        
    }
}