﻿$(document).ready(function () {

        App = new Global.Application();
        App.Service = new Services.ApplicationService();
        App.Utils = new Services.Utils();
        App.Config = new Services.Config();

        App.Service.getResources()
            .then(() => {
                var rootVm = new Vms.AdminRootVm();
                ko.applyBindings(rootVm, $("#root")[0]);
            });  
});