var WcfWrap;
(function (WcfWrap) {
    var DocumentTypesService = /** @class */ (function () {
        function DocumentTypesService() {
            this.wcfService = new WcfServices.IWcfDocumentTypesService();
        }
        DocumentTypesService.prototype.GetDocumentTypes = function () {
            var dfd = $.Deferred();
            this.wcfService.GetDocumentTypes(function (docTypes) {
                dfd.resolve(docTypes);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return DocumentTypesService;
    }());
    WcfWrap.DocumentTypesService = DocumentTypesService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=DocumentTypesService.js.map