﻿namespace WcfWrap {
    export class DocumentTypesService {
        private wcfService = new WcfServices.IWcfDocumentTypesService();

        public GetDocumentTypes(): JQueryPromise<Models.DocumentType[]> {
            let dfd: JQueryDeferred<Models.DocumentType[]> = $.Deferred();

            this.wcfService.GetDocumentTypes((docTypes: Models.DocumentType[]) => {
                dfd.resolve(docTypes);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        }
    }
}