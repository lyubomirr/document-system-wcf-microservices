var WcfWrap;
(function (WcfWrap) {
    var AuthenticationService = /** @class */ (function () {
        function AuthenticationService() {
            this.wcfService = new WcfServices.IWcfAuthenticationService();
        }
        AuthenticationService.prototype.IsAdmin = function () {
            var dfd = $.Deferred();
            this.wcfService.IsAdmin(function (isAdmin) {
                dfd.resolve(isAdmin);
            }, function (err) {
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return AuthenticationService;
    }());
    WcfWrap.AuthenticationService = AuthenticationService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=AuthenticationService.js.map