﻿namespace WcfWrap {
    export class UsersService {
        private wcfService = new WcfServices.IWcfUsersService();

        public GetCurrentUser(): JQueryPromise<Models.User>  {
            let dfd: JQueryDeferred<Models.User> = $.Deferred();
            this.wcfService.GetCurrentUser((user: Models.User) => {
                dfd.resolve(user);
            }, () => {
                dfd.reject(false);
            });
            return dfd.promise();
        }

        public Login(loggedUser: Models.User): JQueryPromise<Models.User>  {
            let dfd: JQueryDeferred<Models.User> = $.Deferred();
            this.wcfService.Login(loggedUser, (user: Models.User) => {
                dfd.resolve(user);
            }, (err: WcfServices.WcfErrorContract) => {
                dfd.reject(err);
                console.log(err);
            });
            return dfd.promise();
        }

        public Logout(): JQueryPromise<void>  {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.Logout(() => {
                dfd.resolve();
            }, () => {
                dfd.reject();
            });
            return dfd.promise();
        }

        public Register(registeringUser: Models.User): JQueryPromise<void>  {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.Register(registeringUser, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                dfd.reject(err);
            });
            return dfd.promise();
        }

        public GetUnconfirmedUsers(searchQuery: Models.SearchQuery): JQueryPromise<Models.User[]> {
            let dfd: JQueryDeferred<Models.User[]> = $.Deferred();
            this.wcfService.GetUnconfirmedUsers(searchQuery, (users: Models.User[]) => {
                dfd.resolve(users);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        }

        public GetUnconfirmedCount(searchQuery?: Models.SearchQuery): JQueryPromise<number> {
            let dfd: JQueryDeferred<number> = $.Deferred();
            this.wcfService.GetUnconfirmedCount(searchQuery, (count: number) => {
                dfd.resolve(count);
            }, (err: WcfServices.WcfErrorContract) => {             
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        }

        public ApproveUser(userGuid: string, roleName: string): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.ApproveUser(userGuid, roleName, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        }

        public RejectUser(userGuid: string): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.RejectUser(userGuid, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        }
    }
}