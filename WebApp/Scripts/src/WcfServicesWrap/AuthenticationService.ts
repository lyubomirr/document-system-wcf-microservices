﻿namespace WcfWrap {
    export class AuthenticationService {
        private wcfService = new WcfServices.IWcfAuthenticationService();
        
        public IsAdmin(): JQueryPromise<boolean> {
            let dfd: JQueryDeferred<boolean> = $.Deferred();

            this.wcfService.IsAdmin((isAdmin: boolean) => {
                dfd.resolve(isAdmin);
            }, (err: WcfServices.WcfErrorContract) => {
                dfd.reject(err);
            });
            return dfd.promise();
        }
    }
}