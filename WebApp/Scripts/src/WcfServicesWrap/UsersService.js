var WcfWrap;
(function (WcfWrap) {
    var UsersService = /** @class */ (function () {
        function UsersService() {
            this.wcfService = new WcfServices.IWcfUsersService();
        }
        UsersService.prototype.GetCurrentUser = function () {
            var dfd = $.Deferred();
            this.wcfService.GetCurrentUser(function (user) {
                dfd.resolve(user);
            }, function () {
                dfd.reject(false);
            });
            return dfd.promise();
        };
        UsersService.prototype.Login = function (loggedUser) {
            var dfd = $.Deferred();
            this.wcfService.Login(loggedUser, function (user) {
                dfd.resolve(user);
            }, function (err) {
                dfd.reject(err);
                console.log(err);
            });
            return dfd.promise();
        };
        UsersService.prototype.Logout = function () {
            var dfd = $.Deferred();
            this.wcfService.Logout(function () {
                dfd.resolve();
            }, function () {
                dfd.reject();
            });
            return dfd.promise();
        };
        UsersService.prototype.Register = function (registeringUser) {
            var dfd = $.Deferred();
            this.wcfService.Register(registeringUser, function () {
                dfd.resolve();
            }, function (err) {
                dfd.reject(err);
            });
            return dfd.promise();
        };
        UsersService.prototype.GetUnconfirmedUsers = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetUnconfirmedUsers(searchQuery, function (users) {
                dfd.resolve(users);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        UsersService.prototype.GetUnconfirmedCount = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetUnconfirmedCount(searchQuery, function (count) {
                dfd.resolve(count);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        UsersService.prototype.ApproveUser = function (userGuid, roleName) {
            var dfd = $.Deferred();
            this.wcfService.ApproveUser(userGuid, roleName, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        UsersService.prototype.RejectUser = function (userGuid) {
            var dfd = $.Deferred();
            this.wcfService.RejectUser(userGuid, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return UsersService;
    }());
    WcfWrap.UsersService = UsersService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=UsersService.js.map