var WcfWrap;
(function (WcfWrap) {
    var RolesService = /** @class */ (function () {
        function RolesService() {
            this.wcfService = new WcfServices.IWcfRolesService();
        }
        RolesService.prototype.GetRoles = function () {
            var dfd = $.Deferred();
            this.wcfService.GetRoles(function (roles) {
                dfd.resolve(roles);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return RolesService;
    }());
    WcfWrap.RolesService = RolesService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=RolesService.js.map