var WcfWrap;
(function (WcfWrap) {
    var DocumentsService = /** @class */ (function () {
        function DocumentsService() {
            this.wcfService = new WcfServices.IWcfDocumentsService();
        }
        DocumentsService.prototype.GetDocuments = function (fcGuid, searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetDocuments(fcGuid, searchQuery, function (docs) {
                dfd.resolve(docs);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        ;
        DocumentsService.prototype.GetArchive = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetArchive(searchQuery, function (docs) {
                dfd.resolve(docs);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        ;
        DocumentsService.prototype.GetDeleted = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetDeleted(searchQuery, function (docs) {
                dfd.resolve(docs);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        ;
        DocumentsService.prototype.GetCount = function (fcGuid, searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetCount(fcGuid, searchQuery, function (count) {
                dfd.resolve(count);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.GetDeletedCount = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetDeletedCount(searchQuery, function (count) {
                dfd.resolve(count);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.GetArchiveCount = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetArchiveCount(searchQuery, function (count) {
                dfd.resolve(count);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.Delete = function (guids) {
            var dfd = $.Deferred();
            this.wcfService.Delete(guids, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject();
            });
            return dfd.promise();
        };
        DocumentsService.prototype.Update = function (doc) {
            var dfd = $.Deferred();
            this.wcfService.Update(doc, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.Archivate = function (guid) {
            var dfd = $.Deferred();
            this.wcfService.Archivate(guid, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.ArchivateFileCabinet = function (fcGuid) {
            var dfd = $.Deferred();
            this.wcfService.ArchivateFileCabinet(fcGuid, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.RestoreArchivated = function (guids) {
            var dfd = $.Deferred();
            this.wcfService.RestoreArchivated(guids, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.RestoreAllArchivated = function () {
            var dfd = $.Deferred();
            this.wcfService.RestoreAllArchivated(function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.RestoreDeleted = function (guids) {
            var dfd = $.Deferred();
            this.wcfService.RestoreDeleted(guids, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.RestoreAllDeleted = function () {
            var dfd = $.Deferred();
            this.wcfService.RestoreAllDeleted(function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.Copy = function (doc) {
            var dfd = $.Deferred();
            this.wcfService.Copy(doc, function (newDoc) {
                dfd.resolve(newDoc);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.CompleteUpload = function (fileInfo) {
            var dfd = $.Deferred();
            this.wcfService.CompleteUpload(fileInfo, function (filePath) {
                dfd.resolve(filePath);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject();
            });
            return dfd.promise();
        };
        DocumentsService.prototype.Add = function (doc) {
            var dfd = $.Deferred();
            this.wcfService.Add(doc, function (returnedDoc) {
                dfd.resolve(returnedDoc);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.CompleteImport = function (importInfo) {
            var dfd = $.Deferred();
            this.wcfService.CompleteImport(importInfo, function (docs) {
                dfd.resolve(docs);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        DocumentsService.prototype.GetRandomGuid = function () {
            var dfd = $.Deferred();
            this.wcfService.GetRandomGuid(function (guid) {
                dfd.resolve(guid);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return DocumentsService;
    }());
    WcfWrap.DocumentsService = DocumentsService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=DocumentsService.js.map