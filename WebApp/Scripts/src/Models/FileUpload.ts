﻿namespace Models {
    export class FileUpload {
        constructor(
            public Guid: string,
            public FileName: string,
            public FileStream: Blob | File
        ) { }
    }
}