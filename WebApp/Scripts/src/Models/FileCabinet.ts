﻿namespace Models {
    export class FileCabinet {
        constructor(
            public Guid: string = "",
            public Name: string = "",
            public Deleted: boolean = false
        ) { }
    }
}