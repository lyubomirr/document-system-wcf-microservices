var Models;
(function (Models) {
    var Document = /** @class */ (function () {
        function Document(Guid, Name, Description, TypeId, StoreDate, Amount, Vat, TotalAmount, LastModifiedUser, LastModifiedDate, Archived, Deleted, FileCabinetId, FilePath, CopyFromDocumentId, Subject, OwnerId, Owner, RegDocNumber, Contact, Company) {
            if (Guid === void 0) { Guid = null; }
            if (Name === void 0) { Name = ""; }
            if (Description === void 0) { Description = ""; }
            if (TypeId === void 0) { TypeId = null; }
            if (StoreDate === void 0) { StoreDate = new Date(); }
            if (Amount === void 0) { Amount = 0; }
            if (Vat === void 0) { Vat = 0; }
            if (TotalAmount === void 0) { TotalAmount = 0; }
            if (LastModifiedUser === void 0) { LastModifiedUser = null; }
            if (LastModifiedDate === void 0) { LastModifiedDate = new Date(); }
            if (Archived === void 0) { Archived = false; }
            if (Deleted === void 0) { Deleted = false; }
            if (FileCabinetId === void 0) { FileCabinetId = null; }
            if (FilePath === void 0) { FilePath = ""; }
            if (CopyFromDocumentId === void 0) { CopyFromDocumentId = null; }
            if (Subject === void 0) { Subject = ""; }
            if (OwnerId === void 0) { OwnerId = null; }
            if (Owner === void 0) { Owner = new Models.User(); }
            if (RegDocNumber === void 0) { RegDocNumber = ""; }
            if (Contact === void 0) { Contact = ""; }
            if (Company === void 0) { Company = ""; }
            this.Guid = Guid;
            this.Name = Name;
            this.Description = Description;
            this.TypeId = TypeId;
            this.StoreDate = StoreDate;
            this.Amount = Amount;
            this.Vat = Vat;
            this.TotalAmount = TotalAmount;
            this.LastModifiedUser = LastModifiedUser;
            this.LastModifiedDate = LastModifiedDate;
            this.Archived = Archived;
            this.Deleted = Deleted;
            this.FileCabinetId = FileCabinetId;
            this.FilePath = FilePath;
            this.CopyFromDocumentId = CopyFromDocumentId;
            this.Subject = Subject;
            this.OwnerId = OwnerId;
            this.Owner = Owner;
            this.RegDocNumber = RegDocNumber;
            this.Contact = Contact;
            this.Company = Company;
        }
        return Document;
    }());
    Models.Document = Document;
})(Models || (Models = {}));
//# sourceMappingURL=Document.js.map