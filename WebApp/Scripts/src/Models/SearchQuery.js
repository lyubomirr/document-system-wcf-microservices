var Models;
(function (Models) {
    var SearchQuery = /** @class */ (function () {
        function SearchQuery(SearchValue, PageNumber, ElementsPerPage, SortProperty, IsAscending, Type) {
            if (SearchValue === void 0) { SearchValue = null; }
            if (PageNumber === void 0) { PageNumber = null; }
            if (ElementsPerPage === void 0) { ElementsPerPage = null; }
            if (SortProperty === void 0) { SortProperty = null; }
            if (IsAscending === void 0) { IsAscending = null; }
            if (Type === void 0) { Type = null; }
            this.SearchValue = SearchValue;
            this.PageNumber = PageNumber;
            this.ElementsPerPage = ElementsPerPage;
            this.SortProperty = SortProperty;
            this.IsAscending = IsAscending;
            this.Type = Type;
        }
        return SearchQuery;
    }());
    Models.SearchQuery = SearchQuery;
})(Models || (Models = {}));
//# sourceMappingURL=SearchQuery.js.map