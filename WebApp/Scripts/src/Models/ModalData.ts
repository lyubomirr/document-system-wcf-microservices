﻿namespace Models {
    export class ModalData {
        constructor(
            public titleKey: string,
            public textKey: string,
            public cb: CallbackFunction,
            public name?: string) { }
    }
}