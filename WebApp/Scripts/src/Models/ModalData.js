var Models;
(function (Models) {
    var ModalData = /** @class */ (function () {
        function ModalData(titleKey, textKey, cb, name) {
            this.titleKey = titleKey;
            this.textKey = textKey;
            this.cb = cb;
            this.name = name;
        }
        return ModalData;
    }());
    Models.ModalData = ModalData;
})(Models || (Models = {}));
//# sourceMappingURL=ModalData.js.map