﻿namespace Models {
    export class Document {
        constructor(
            public Guid: string = null,
            public Name: string = "",
            public Description: string = "",
            public TypeId: string = null,
            public StoreDate: Date = new Date(),
            public Amount: number = 0,
            public Vat: number = 0,
            public TotalAmount: number = 0,
            public LastModifiedUser: string = null,
            public LastModifiedDate: Date = new Date(),
            public Archived: boolean = false,
            public Deleted: boolean = false,
            public FileCabinetId: string = null,
            public FilePath: string = "",
            public CopyFromDocumentId: string = null,
            public Subject: string = "",
            public OwnerId: string = null,
            public Owner: User = new Models.User(),
            public RegDocNumber: string = "",
            public Contact: string = "",
            public Company: string = ""
        ) { }
    }
}