﻿interface ArbiterContract {
    subscribe(event: string, cb: CallbackFunction, options?: Object);
    unsubscribe(event: string);
    publish(event: string, data?: any, options?: Object);
}
declare var Arbiter: ArbiterContract;