var Vms;
(function (Vms) {
    var AdminPanelVm = /** @class */ (function () {
        function AdminPanelVm() {
            var _this = this;
            this.unconfirmedUsers = ko.observableArray([]);
            this.searchQuery = ko.observable(new DataVms.SearchQueryViewModel());
            this.selectedModalUser = ko.observable();
            this.approvedUserType = ko.observable();
            this.modalTitle = ko.observable("");
            this.modalText = ko.observable("");
            this.modalCb = function () { };
            this.searchQuery().SortProperty("Username");
            ko.computed(function () {
                _this.searchQuery().ElementsPerPage();
                _this.searchQuery().IsAscending();
                _this.searchQuery().PageNumber();
                _this.searchQuery().SearchValue();
                if (!ko.computedContext.isInitial()) {
                    _this.getCurrentCount()
                        .then(function (count) {
                        App.Config.currentDataCount(count);
                        _this.loadUnconfirmedUsers();
                    });
                }
            });
            this.maxPageNumber = ko.computed(function () {
                return Math.ceil(App.Config.currentDataCount() / _this.searchQuery().ElementsPerPage());
            });
            this.loadUnconfirmedUsers();
        }
        AdminPanelVm.prototype.getTemplateName = function () {
            return "admin-panel-template";
        };
        AdminPanelVm.prototype.loadUnconfirmedUsers = function () {
            var _this = this;
            App.Service.wcf.usersService.GetUnconfirmedUsers(this.searchQuery().toModel())
                .then(function (users) {
                _this.unconfirmedUsers([]);
                for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
                    var user = users_1[_i];
                    _this.unconfirmedUsers.push(new DataVms.UserViewModel(user));
                }
            });
        };
        AdminPanelVm.prototype.getCurrentCount = function () {
            return App.Service.wcf.usersService.GetUnconfirmedCount(this.searchQuery().toModel());
        };
        AdminPanelVm.prototype.sortToggle = function () {
            this.searchQuery().IsAscending(!this.searchQuery().IsAscending());
        };
        AdminPanelVm.prototype.rejectUser = function (user) {
            var _this = this;
            App.Service.wcf.usersService.RejectUser(user.Guid)
                .then(function () {
                _this.unconfirmedUsers.remove(user);
            });
        };
        AdminPanelVm.prototype.approveUser = function () {
            var _this = this;
            App.Service.wcf.usersService.ApproveUser(this.selectedModalUser().Guid, this.approvedUserType())
                .then(function () {
                _this.unconfirmedUsers.remove(_this.selectedModalUser());
                _this.approvedUserType("");
                _this.selectedModalUser(new DataVms.UserViewModel());
                App.Utils.hideModal("#approve-user-modal");
            });
        };
        AdminPanelVm.prototype.openApproveModal = function (user) {
            this.selectedModalUser(user);
            App.Utils.showModal("#approve-user-modal");
        };
        AdminPanelVm.prototype.getNextPage = function () {
            if (this.searchQuery().PageNumber() < this.maxPageNumber()) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() + 1);
            }
        };
        AdminPanelVm.prototype.getPrevPage = function () {
            if (this.searchQuery().PageNumber() > 1) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() - 1);
            }
        };
        AdminPanelVm.prototype.openConfirmModal = function (titleKey, textKey, cb, name) {
            App.Utils.showModal("#confirm-modal");
            this.modalTitle(App.Service.locResources[titleKey]);
            if (name) {
                this.modalText(App.Service.locResources[textKey] + ' "' + name + '" ?');
            }
            else {
                this.modalText(App.Service.locResources[textKey]);
            }
            this.modalCb = cb;
        };
        return AdminPanelVm;
    }());
    Vms.AdminPanelVm = AdminPanelVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=AdminPanelVm.js.map