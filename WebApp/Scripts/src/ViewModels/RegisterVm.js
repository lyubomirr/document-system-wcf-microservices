var Vms;
(function (Vms) {
    var RegisterVm = /** @class */ (function () {
        function RegisterVm() {
            this.registeringUser = ko.observable(new DataVms.UserViewModel());
            this.confirmPassword = ko.observable();
            this.successfulRegistration = ko.observable(false);
        }
        RegisterVm.prototype.getTemplateName = function () {
            return "register-template";
        };
        RegisterVm.prototype.register = function () {
            var _this = this;
            if (!App.Utils.isInputValid(this.registeringUser().Username())) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            if (this.registeringUser().Password() != this.confirmPassword()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['NoMatchingPasswords']);
                return;
            }
            App.Service.wcf.usersService.Register(this.registeringUser().toModel())
                .then(function () {
                _this.successfulRegistration(true);
                _this.confirmPassword(null);
                _this.registeringUser(new DataVms.UserViewModel());
            }, function (err) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources[err.get_message()]);
            });
        };
        return RegisterVm;
    }());
    Vms.RegisterVm = RegisterVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=RegisterVm.js.map