var Vms;
(function (Vms) {
    var RootVm = /** @class */ (function () {
        function RootVm() {
            var _this = this;
            this.currentActionVm = ko.observable();
            App.Service.wcf.usersService.GetCurrentUser()
                .then(function (user) {
                App.Service.currentUser(user);
                _this.initApp();
            }, function () {
                _this.currentActionVm(new Vms.LoginVm());
            });
            Arbiter.subscribe("logged", function () {
                _this.initApp();
            });
            Arbiter.subscribe("unauthorized", function () {
                _this.currentActionVm(new Vms.LoginVm());
            });
            Arbiter.subscribe("goToRegister", function () {
                _this.currentActionVm(new Vms.RegisterVm());
            });
        }
        RootVm.prototype.initApp = function () {
            var _this = this;
            $.when(App.Service.init())
                .then(function () {
                _this.currentActionVm(new Vms.ApplicationVm());
            });
        };
        RootVm.prototype.logout = function () {
            var _this = this;
            App.Service.wcf.usersService.Logout()
                .then(function () {
                App.Service.currentUser(null);
                _this.currentActionVm(new Vms.LoginVm());
            });
        };
        return RootVm;
    }());
    Vms.RootVm = RootVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=RootVm.js.map