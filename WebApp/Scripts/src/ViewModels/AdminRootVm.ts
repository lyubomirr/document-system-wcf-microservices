﻿namespace Vms {
    export class AdminRootVm {
        private currentActionVm: KnockoutObservable<ActionVm> = ko.observable(); 

        constructor() {
            this.setAppropriateView();
         
            Arbiter.subscribe("logged", () => {
                this.setAppropriateView();
            });
        }

        public initAdminPanel(): void {
            $.when(App.Service.init())
                .then(() => {
                    this.currentActionVm(new AdminPanelVm());
                });
        }

        private logout(): void {
            App.Service.wcf.usersService.Logout()
                .then(() => {
                    App.Service.currentUser(null);
                    this.currentActionVm(new LoginVm());
                });
        }

        private setAppropriateView() {
            App.Service.wcf.authService.IsAdmin()
                .then((isAdmin: boolean) => {
                    if (isAdmin) {
                        App.Service.wcf.usersService.GetCurrentUser()
                            .then((user: Models.User) => {
                                App.Service.currentUser(user);
                                this.initAdminPanel();
                            });
                    } else {
                        this.logout();
                    }
                });
        }
    }
}