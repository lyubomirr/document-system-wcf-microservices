﻿namespace Vms {
    export class RegisterVm implements ActionVm {
        private registeringUser: KnockoutObservable<DataVms.UserViewModel> = ko.observable(new DataVms.UserViewModel());
        private confirmPassword: KnockoutObservable<string> = ko.observable();
        private successfulRegistration: KnockoutObservable<boolean> = ko.observable(false);

        getTemplateName(): string {
            return "register-template";
        }

        private register(): void {
            if (!App.Utils.isInputValid(this.registeringUser().Username())) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }

            if (this.registeringUser().Password() != this.confirmPassword()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['NoMatchingPasswords']);
                return;
            }

            App.Service.wcf.usersService.Register(this.registeringUser().toModel())
                .then(() => {
                    this.successfulRegistration(true);
                    this.confirmPassword(null);
                    this.registeringUser(new DataVms.UserViewModel());
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources[err.get_message()]);
                })
        }
    }
}