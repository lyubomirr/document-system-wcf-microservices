﻿namespace Vms {
    export class AdminPanelVm implements ActionVm {
        private unconfirmedUsers: KnockoutObservableArray<DataVms.UserViewModel> = ko.observableArray([]);
        private searchQuery: KnockoutObservable<DataVms.SearchQueryViewModel>
            = ko.observable(new DataVms.SearchQueryViewModel());

        private maxPageNumber: KnockoutComputed<number>;

        private selectedModalUser: KnockoutObservable<DataVms.UserViewModel> = ko.observable();
        private approvedUserType: KnockoutObservable<string> = ko.observable();

        private modalTitle: KnockoutObservable<String> = ko.observable("");
        private modalText: KnockoutObservable<String> = ko.observable("");
        private modalCb: CallbackFunction = () => { };
         
        constructor() {

            this.searchQuery().SortProperty("Username");

            ko.computed(() => {
                this.searchQuery().ElementsPerPage();
                this.searchQuery().IsAscending();
                this.searchQuery().PageNumber();
                this.searchQuery().SearchValue();

                if (!ko.computedContext.isInitial()) {
                    this.getCurrentCount()
                        .then((count: number) => {
                            App.Config.currentDataCount(count);
                            this.loadUnconfirmedUsers();
                        });
                }
            });

            this.maxPageNumber = ko.computed(() => {
                return Math.ceil(App.Config.currentDataCount() / this.searchQuery().ElementsPerPage());
            });

            this.loadUnconfirmedUsers();
        }


        getTemplateName(): string {
            return "admin-panel-template";
        }

        private loadUnconfirmedUsers(): void {
            App.Service.wcf.usersService.GetUnconfirmedUsers(this.searchQuery().toModel())
                .then((users: Models.User[]) => {
                    this.unconfirmedUsers([]);
                    for (let user of users) {
                        this.unconfirmedUsers.push(new DataVms.UserViewModel(user));
                    }
                });
        }

        private getCurrentCount(): JQueryPromise<number> {
            return App.Service.wcf.usersService.GetUnconfirmedCount(this.searchQuery().toModel());
        }

        private sortToggle() {
            this.searchQuery().IsAscending(!this.searchQuery().IsAscending());
        }

        private rejectUser(user: DataVms.UserViewModel): void {
            App.Service.wcf.usersService.RejectUser(user.Guid)
                .then(() => {
                    this.unconfirmedUsers.remove(user);
                });
        }

        private approveUser(): void {
            App.Service.wcf.usersService.ApproveUser(this.selectedModalUser().Guid, this.approvedUserType())
                .then(() => {
                    this.unconfirmedUsers.remove(this.selectedModalUser());
                    this.approvedUserType("");
                    this.selectedModalUser(new DataVms.UserViewModel());
                    App.Utils.hideModal("#approve-user-modal");
                })
        }

        private openApproveModal(user: DataVms.UserViewModel): void {
            this.selectedModalUser(user);
            App.Utils.showModal("#approve-user-modal");
        }

        private getNextPage(): void {
            if (this.searchQuery().PageNumber() < this.maxPageNumber()) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() + 1);
            }
        }

        private getPrevPage(): void {
            if (this.searchQuery().PageNumber() > 1) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() - 1);
            }
        }

        private openConfirmModal(titleKey: string, textKey: string, cb: CallbackFunction, name?: string): void {
            App.Utils.showModal("#confirm-modal");
            this.modalTitle(App.Service.locResources[titleKey]);
            if (name) {
                this.modalText(App.Service.locResources[textKey] + ' "' + name + '" ?');
            } else {
                this.modalText(App.Service.locResources[textKey]);
            }
            this.modalCb = cb;
        }     
    
    }
}