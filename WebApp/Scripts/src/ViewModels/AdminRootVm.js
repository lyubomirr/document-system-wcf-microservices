var Vms;
(function (Vms) {
    var AdminRootVm = /** @class */ (function () {
        function AdminRootVm() {
            var _this = this;
            this.currentActionVm = ko.observable();
            this.setAppropriateView();
            Arbiter.subscribe("logged", function () {
                _this.setAppropriateView();
            });
        }
        AdminRootVm.prototype.initAdminPanel = function () {
            var _this = this;
            $.when(App.Service.init())
                .then(function () {
                _this.currentActionVm(new Vms.AdminPanelVm());
            });
        };
        AdminRootVm.prototype.logout = function () {
            var _this = this;
            App.Service.wcf.usersService.Logout()
                .then(function () {
                App.Service.currentUser(null);
                _this.currentActionVm(new Vms.LoginVm());
            });
        };
        AdminRootVm.prototype.setAppropriateView = function () {
            var _this = this;
            App.Service.wcf.authService.IsAdmin()
                .then(function (isAdmin) {
                if (isAdmin) {
                    App.Service.wcf.usersService.GetCurrentUser()
                        .then(function (user) {
                        App.Service.currentUser(user);
                        _this.initAdminPanel();
                    });
                }
                else {
                    _this.logout();
                }
            });
        };
        return AdminRootVm;
    }());
    Vms.AdminRootVm = AdminRootVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=AdminRootVm.js.map