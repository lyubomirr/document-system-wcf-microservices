﻿declare module WcfServices {
    export class WcfErrorContract {
        get_message();
        get_statusCode();
    }
}