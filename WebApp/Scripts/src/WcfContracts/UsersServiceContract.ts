﻿declare module WcfServices {
    export class IWcfUsersService {
        GetCurrentUser(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Login(loggedUser: Models.User, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Logout(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Register(registeringUser: Models.User, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        GetUnconfirmedUsers(searchQuery: Models.SearchQuery, succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

        GetUnconfirmedCount(searchQuery: Models.SearchQuery, succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

        ApproveUser(userGuid: string, roleName: string, succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

        RejectUser(userGuid: string, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
    }
}