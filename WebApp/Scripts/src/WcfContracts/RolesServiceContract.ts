﻿declare module WcfServices {
    export class IWcfRolesService {
        GetRoles(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
    }
}