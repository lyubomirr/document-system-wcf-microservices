﻿declare module WcfServices {
    export class IWcfDocumentTypesService {
        GetDocumentTypes(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
    }
}