﻿type CallbackFunction = (...args: any[]) => any;
type AsyncFunction = (...args: any[]) => JQueryPromise<any>;
type Dictionary<T> = { [key: string]: T };