﻿ko.bindingHandlers.customFileInput = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).on('change', function () {
            let fileName = ($(element) as any)[0].files[0].name;
            $(this).next('.custom-file-label').text(fileName);
        });
    }
}

ko.bindingHandlers.resetModalForm = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).on('hidden.bs.modal', function () {
            $(this).find('form')[0].reset();
            $(this).find('.custom-file-label').text(App.Service.locResources['ChooseFile']);
        });
    } 
}

ko.bindingHandlers.setModalSubmitCallback = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).on("click", () => {
            $(valueAccessor().modalSelector).find("form").on("submit", function (e) {
                e.preventDefault();
                valueAccessor().cb.apply(valueAccessor().ctx, valueAccessor().cbArgs);
            });
        });
    }
};

ko.bindingHandlers.initComponent = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $('[data-toggle="tooltip"]').tooltip();
    }
}