var Api;
(function (Api) {
    var DataMap = /** @class */ (function () {
        function DataMap(roles, documentTypes) {
            this.roles = new Array();
            this.documentTypes = new Array();
            this.roles = roles;
            this.documentTypes = documentTypes;
        }
        DataMap.prototype.getRole = function (guid) {
            for (var _i = 0, _a = this.roles; _i < _a.length; _i++) {
                var role = _a[_i];
                if (role.Guid === guid) {
                    return role;
                }
            }
        };
        DataMap.prototype.getDocumentType = function (guid) {
            for (var _i = 0, _a = this.documentTypes; _i < _a.length; _i++) {
                var docType = _a[_i];
                if (docType.Guid === guid) {
                    return docType;
                }
            }
            return new Models.DocumentType();
        };
        DataMap.prototype.getDocumentTypeName = function (guid) {
            for (var _i = 0, _a = this.documentTypes; _i < _a.length; _i++) {
                var docType = _a[_i];
                if (docType.Guid === guid) {
                    return docType.Name;
                }
            }
            return "";
        };
        return DataMap;
    }());
    Api.DataMap = DataMap;
})(Api || (Api = {}));
//# sourceMappingURL=DataMap.js.map