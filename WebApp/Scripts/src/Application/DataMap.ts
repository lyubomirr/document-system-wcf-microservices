﻿namespace Api {
    export class DataMap {
        private roles: Models.Role[] = new Array<Models.Role>();
        private documentTypes: Models.DocumentType[] = new Array<Models.DocumentType>();

        constructor(roles: Models.Role[], documentTypes: Models.Role[]) {
            this.roles = roles;
            this.documentTypes = documentTypes;
        }

        public getRole(guid: string): Models.Role {
            for (let role of this.roles) {
                if (role.Guid === guid) {
                    return role;
                }
            }
        }

        public getDocumentType(guid: string): Models.DocumentType {
            for (let docType of this.documentTypes) {
                if (docType.Guid === guid) {
                    return docType;
                }
            }

            return new Models.DocumentType();
        }

        public getDocumentTypeName(guid: string): string {
            for (let docType of this.documentTypes) {
                if (docType.Guid === guid) {
                    return docType.Name;
                }
            }
            return "";
        }
    }
}