﻿namespace Api {
    export class Endpoints {
        public readonly exportFileCabinet: string = "/FileDownloadService/WcfServiceHost.svc/ExportFileCabinet";
        public readonly exportDocuments: string = "/FileDownloadService/WcfServiceHost.svc/ExportDocuments";   
        public readonly getFile: string = "/FileDownloadService/WcfServiceHost.svc/GetFile";     
        public readonly uploadChunks: string = "Documents/UploadChunks/";
        public readonly getCurrentResources: string = "/DocumentSystem/home/getcurrentresources";
    }
}