﻿namespace Services {
    export class Config {
        public readonly supportedPreviewExtensions: string[] = [".pdf", ".png", ".jpg", ".jpeg", ".bmp", ".txt", ".gif", ".js", ".css"];
        public currentDataCount: KnockoutObservable<number> = ko.observable(0);
        public readonly pageElementsAvailableOptions: number[] = [1, 2, 5, 10, 20, 50, 100];
        public readonly bytesPerUploadChunk: number = 4000000; //4000000
    }
}