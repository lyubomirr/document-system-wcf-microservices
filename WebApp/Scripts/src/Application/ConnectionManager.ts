﻿namespace Api {
    export class ConnectionManager {
        private getJsonRequest(apiUrl: string): JQueryPromise<any> {
            return $.ajax({
                method: 'GET',
                url: apiUrl,
                contentType: "application/json"
            });
        }

        private postJsonRequest<T>(apiUrl: string, sentData: T): JQueryPromise<any> {
            return $.post(apiUrl, sentData);
        }

        public invokeGet<T>(apiUrl: string, query?: any): JQueryPromise<T> {
            let dfd = $.Deferred<T>();

            let finalUrl = apiUrl;
            if (query) {
                finalUrl += query;
            }

            this.getJsonRequest(finalUrl)
                .then((data) => {
                    dfd.resolve(data);
                });

            return dfd.promise();
        }

        public invokePost<T>(apiUrl: string, sentData: T, query?: any): JQueryPromise<T> {
            let dfd = $.Deferred<T>();

            let finalUrl = apiUrl;
            if (query) {
                finalUrl += query;
            }

            this.postJsonRequest(finalUrl, sentData)
                .then((data) => {
                    dfd.resolve(data);
                });

            return dfd.promise();
        }

        public invokePostDifferent<T, U>(apiUrl: string, sentData: T, query?: any): JQueryPromise<U> {
            let dfd = $.Deferred<U>();

            let finalUrl = apiUrl;
            if (query) {
                finalUrl += query;
            }

            this.postJsonRequest(finalUrl, sentData)
                .then((data) => {
                    dfd.resolve(data);
                });

            return dfd.promise();
        }

        public uploadFileByChunks(uploadChunkUrl: string, blob: File): JQueryPromise<string> {
            let dfd = $.Deferred();
            let fileSize: number = blob.size;

            let startChunk: number = 0;
            let endChunk: number = App.Config.bytesPerUploadChunk;

            $(".progress").slideDown();
            this.uploadChunks(uploadChunkUrl, startChunk, endChunk, blob, dfd);
            return dfd.promise();
        }

        private uploadChunks(url: string, startChunk: number, endChunk: number,
            blob: File, dfd: JQueryDeferred<string>) {

            if (startChunk >= blob.size) {
                dfd.resolve(blob.name);
                return;
            }

            let chunk = blob.slice(startChunk, endChunk);
            let fd = new FormData();
            fd.append("chunk", chunk);
            fd.append("fileName", blob.name);

            $.ajax({
                url: url,
                type: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                success: () => {
                    var percentComplete = (endChunk / blob.size) * 100;
                    $(".upload-file-progress").attr('aria-valuenow', percentComplete).css('width', percentComplete + "%");
                    startChunk = endChunk;
                    endChunk += App.Config.bytesPerUploadChunk;
                    this.uploadChunks(url, startChunk, endChunk, blob, dfd);
                },
                error: () => {
                    dfd.reject();
                    return;
                }
            });
        }
    }
}