var Api;
(function (Api) {
    var WcfConnectionManager = /** @class */ (function () {
        function WcfConnectionManager() {
            this.documentsService = new WcfWrap.DocumentsService();
            this.usersService = new WcfWrap.UsersService();
            this.fileCabinetsService = new WcfWrap.FileCabinetsService();
            this.rolesService = new WcfWrap.RolesService();
            this.docTypesService = new WcfWrap.DocumentTypesService();
            this.authService = new WcfWrap.AuthenticationService();
        }
        return WcfConnectionManager;
    }());
    Api.WcfConnectionManager = WcfConnectionManager;
    ;
})(Api || (Api = {}));
//# sourceMappingURL=WcfConnectionManager.js.map