var Services;
(function (Services) {
    var ApplicationService = /** @class */ (function () {
        function ApplicationService() {
            this.connManager = new Api.ConnectionManager();
            this.wcf = new Api.WcfConnectionManager();
            this.endpoints = new Api.Endpoints();
            this.currentUser = ko.observable();
        }
        ApplicationService.prototype.getResources = function () {
            var _this = this;
            var dfd = $.Deferred();
            this.connManager.invokeGet(App.Service.endpoints.getCurrentResources)
                .then(function (resources) {
                _this.locResources = resources;
                dfd.resolve();
            });
            return dfd.promise();
        };
        ApplicationService.prototype.init = function () {
            var _this = this;
            var dfd = $.Deferred();
            this.wcf.rolesService.GetRoles()
                .then(function (roles) {
                _this.wcf.docTypesService.GetDocumentTypes()
                    .then(function (docTypes) {
                    for (var _i = 0, docTypes_1 = docTypes; _i < docTypes_1.length; _i++) {
                        var docType = docTypes_1[_i];
                        docType.Name = _this.locResources[docType.Name];
                    }
                    _this.dataMapper = new Api.DataMap(roles, docTypes);
                    dfd.resolve();
                });
            });
            return dfd.promise();
        };
        return ApplicationService;
    }());
    Services.ApplicationService = ApplicationService;
})(Services || (Services = {}));
//# sourceMappingURL=ApplicationService.js.map