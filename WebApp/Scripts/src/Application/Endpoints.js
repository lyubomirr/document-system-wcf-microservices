var Api;
(function (Api) {
    var Endpoints = /** @class */ (function () {
        function Endpoints() {
            this.exportFileCabinet = "/FileDownloadService/WcfServiceHost.svc/ExportFileCabinet";
            this.exportDocuments = "/FileDownloadService/WcfServiceHost.svc/ExportDocuments";
            this.getFile = "/FileDownloadService/WcfServiceHost.svc/GetFile";
            this.uploadChunks = "Documents/UploadChunks/";
            this.getCurrentResources = "/DocumentSystem/home/getcurrentresources";
        }
        return Endpoints;
    }());
    Api.Endpoints = Endpoints;
})(Api || (Api = {}));
//# sourceMappingURL=Endpoints.js.map