﻿namespace DataVms {
    export class SearchQueryViewModel {
        constructor(
            public SearchValue: KnockoutObservable<string> = ko.observable(""),
            public PageNumber: KnockoutObservable<number> = ko.observable(1),
            public ElementsPerPage: KnockoutObservable<number> = ko.observable(5),
            public IsAscending: KnockoutObservable<boolean> = ko.observable(true),
            public SortProperty: KnockoutObservable<string> = ko.observable("Name"),
            public Type: KnockoutObservable<string> = ko.observable())
        {
            this.SearchValue.subscribe(() => {
                this.PageNumber(1);
            });

            this.Type.subscribe(() => {
                this.PageNumber(1);
            })
        }

        toModel(): Models.SearchQuery {
            let model = new Models.SearchQuery(
                this.SearchValue(),
                this.PageNumber(),
                this.ElementsPerPage(),
                this.SortProperty(),
                this.IsAscending(),
                this.Type()
            );

            return model;
        }
    }
}