var DataVms;
(function (DataVms) {
    var FileCabinetViewModel = /** @class */ (function () {
        function FileCabinetViewModel(model) {
            this.Guid = null;
            this.Name = ko.observable("");
            this.Deleted = false;
            if (model) {
                this.Guid = model.Guid;
                this.Name(model.Name);
                this.Deleted = model.Deleted;
            }
        }
        FileCabinetViewModel.prototype.toModel = function () {
            return new Models.FileCabinet(this.Guid, this.Name(), this.Deleted);
        };
        FileCabinetViewModel.prototype.triggerConfirmModal = function (titleKey, textKey, cb, name) {
            Arbiter.publish("openConfirmModal", new Models.ModalData(titleKey, textKey, cb, name));
        };
        FileCabinetViewModel.prototype.triggerFileCabinetChange = function (fileCabinet) {
            Arbiter.publish("fileCabinetChange", fileCabinet);
        };
        return FileCabinetViewModel;
    }());
    DataVms.FileCabinetViewModel = FileCabinetViewModel;
})(DataVms || (DataVms = {}));
//# sourceMappingURL=FileCabinetViewModel.js.map