﻿using Common.Factories;
using Interfaces.Wcf;
using Ninject.Modules;
using System.Configuration;
using System.Web;

namespace WebApp.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            var uploadsFullPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uploadDirectory"]);
            var tempFullPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["tempDirectory"]);


            var channelFactory = new WcfServiceChannelFactory();
            Bind<IWcfDocumentsService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfDocumentsService>("DocumentsServiceEndpoint"));
        }
    }
}