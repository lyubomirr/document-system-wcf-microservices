﻿using System.Web.Mvc;
using System.IO;
using Interfaces.Wcf;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Net;

namespace WebApp.Controllers
{
    public class DocumentsController : Controller
    {
        private IWcfDocumentsService _documentsService;

        public DocumentsController(IWcfDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }

        [HttpPost]
        [Route("documents/uploadchunks/{guid}")]
        public ActionResult UploadChunks(string guid, string fileName)
        {
            if (Request.Files.Count == 0)
            {
                return new EmptyResult();
            }

            byte[] fileToByteArray;

            using (var ms = new MemoryStream())
            {
                Request.Files[0].InputStream.CopyTo(ms);
                fileToByteArray = ms.ToArray();
            }

            var cookieHeader = HttpContext.Request.Headers.Get("Cookie");
            using (new OperationContextScope((IContextChannel)_documentsService))
            {
                WebOperationContext.Current.OutgoingRequest.Headers.Add(HttpRequestHeader.Cookie, cookieHeader);
                _documentsService.UploadChunks(guid, fileName, fileToByteArray);
            }

            return new EmptyResult();
        }
    }
}