﻿using System.Collections;
using System.Globalization;
using System.Web.Mvc;
using System.Linq;
using WebApp.Common;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Mode = AppMode.Application;
            return View();
        }

        public ActionResult AdminPanel()
        {
            ViewBag.Mode = AppMode.AdminPanel;
            return View("Index");
        }
            
        public ActionResult GetCurrentResources()
        {
            return Json(
                Resources.Data.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true)
                .OfType<DictionaryEntry>().ToDictionary(el => el.Key.ToString(), el => el.Value.ToString()), 
                JsonRequestBehavior.AllowGet);
        }
    }
}