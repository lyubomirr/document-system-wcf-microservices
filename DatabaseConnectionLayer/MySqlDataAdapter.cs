﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace DatabaseConnectionLayer
{
    public class MySqlDataAdapter : DatabaseDataAdapter
    {

        public MySqlDataAdapter(string connectionString)
            :base(connectionString) { }

        protected override DbCommand GetDbCommand(string cmdTxt, DbConnection connection)
        {
            if(connection is MySqlConnection mySqlConnection)
            {
                return new MySqlCommand(cmdTxt, mySqlConnection);
            }

            throw new ArgumentException("Wrong db connection!");
        }

        protected override DbConnection GetDbConnection()
        {
            return new MySqlConnection(_connectionString);
        }
    }
}
