﻿using Interfaces.Repositories;
using Interfaces.Services;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;

namespace FileCabinetsService.Services
{
    public class ApplicationService : IFileCabinetsService
    {
        private IFileCabinetsRepository _fileCabinetsRepository;
        private readonly string _uploadsDirectoryPath;

        public ApplicationService(IFileCabinetsRepository fileCabinetsRepository, string uploadsDirectoryPath)
        {
            _fileCabinetsRepository = fileCabinetsRepository;
            _uploadsDirectoryPath = uploadsDirectoryPath;
        }

        public FileCabinet GetFileCabinetById(Guid guid)
        {
            var fc = _fileCabinetsRepository.GetFileCabinetById(guid);
            return fc;
        }

        public IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery = null)
        {
            var fcs = _fileCabinetsRepository.GetFileCabinets(searchQuery);
            return fcs;
        }

        public int GetCount(SearchQuery searchQuery = null)
        {
            int count = _fileCabinetsRepository.GetCount(searchQuery);
            return count;
        }

        public FileCabinet Add(FileCabinet fileCabinet)
        {
            fileCabinet.Guid = Guid.NewGuid();
            var fcDirectoryPath = Path.Combine(_uploadsDirectoryPath, fileCabinet.Guid.ToString());
            Directory.CreateDirectory(fcDirectoryPath);

            _fileCabinetsRepository.AddFileCabinet(fileCabinet);
            return fileCabinet;
        }

        public void Delete(Guid fileCabinetGuid)
        {
            var fileCabinet = _fileCabinetsRepository.GetFileCabinetById(fileCabinetGuid);
            if (!fileCabinet.Deleted)
            {
                fileCabinet.Deleted = true;
                _fileCabinetsRepository.UpdateFileCabinet(fileCabinet);
            }
        }

        public void Update(FileCabinet updated)
        {
            _fileCabinetsRepository.UpdateFileCabinet(updated);
        }
    }
}