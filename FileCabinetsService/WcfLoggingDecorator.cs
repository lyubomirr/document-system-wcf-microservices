﻿using Interfaces.Wcf;
using log4net;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace FileCabinetsService
{
    public class WcfLoggingDecorator : IWcfFileCabinetsService
    {
        private IWcfFileCabinetsService _wcfCore;
        private readonly ILog _logger;

        public WcfLoggingDecorator(IWcfFileCabinetsService wcfCore)
        {
            _wcfCore = wcfCore;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public FileCabinet Add(FileCabinet fileCabinet)
        {
            _logger.Debug($"Add operation called with parameter fileCabinet: {{{fileCabinet}}}.");
            try
            {
                return _wcfCore.Add(fileCabinet);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Add operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Add operation!", ex);
                return null;
            }
        }

        public void Delete(string guid)
        {
            _logger.Debug($"Delete operation called with parameter guid: \"{guid}\".");
            try
            {
                _wcfCore.Delete(guid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Delete operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Delete operation!", ex);
            }
        }

        public int GetCount(SearchQuery searchQuery)
        {
            _logger.Debug($"GetCount operation called with parameter searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetCount(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetCount operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetCount operation!", ex);
                return 0;
            }
        }

        public FileCabinet GetFileCabinetById(string guid)
        {
            _logger.Debug($"GetFileCabinetById operation called with parameter guid: \"{guid}\".");
            try
            {
                return _wcfCore.GetFileCabinetById(guid);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetFileCabinetById operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetFileCabinetById operation!", ex);
                return null;
            }
        }

        public IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery)
        {           
            _logger.Debug($"GetFileCabinets operation called with parameter searchQuery: {{{searchQuery}}}.");
            try
            {
                return _wcfCore.GetFileCabinets(searchQuery);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at GetFileCabinets operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at GetFileCabinets operation!", ex);
                return null;
            }
        }

        public void Update(FileCabinet updated)
        {
            _logger.Debug($"GetFileCabinets operation called with parameter updated: {{{updated}}}.");
            try
            {
                _wcfCore.Update(updated);
            }
            catch (WebFaultException webException)
            {
                _logger.Error("WebFaultException thrown at Update operation!", webException);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception thrown at Update operation!", ex);
            }
        }
    }
}