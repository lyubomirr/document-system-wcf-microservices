﻿using Common.Factories;
using FileCabinetsService.Repositories;
using FileCabinetsService.Services;
using Interfaces.Repositories;
using Interfaces.Services;
using Interfaces.Wcf;
using Ninject.Modules;
using System.Configuration;

namespace FileCabinetsService.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            var uploadsPath = ConfigurationManager.AppSettings["uploadsPath"];

            Bind<IFileCabinetsService>().To<ApplicationService>()
                .WithConstructorArgument("uploadsDirectoryPath", uploadsPath);

            Bind<IFileCabinetsRepository>().To<FileCabinetsRepository>()
               .WithConstructorArgument("tableName", "FileCabinets");

            Bind<IWcfFileCabinetsService>().To<WcfCoreService>()
                .WhenInjectedInto<WcfLoggingDecorator>();

            var channelFactory = new WcfServiceChannelFactory();
            Bind<IWcfAuthenticationService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfAuthenticationService>("AuthEndpoint"));

            Bind<IWcfDocumentsService>()
                .ToMethod(ctx => channelFactory.CreateChannel<IWcfDocumentsService>("DocumentsServiceEndpoint"));
        }
    }
}