﻿using System;
using System.Net;
using System.Linq;
using Models.Dtos;
using Common.Parents;
using Interfaces.Wcf;
using Models.DbEntities;
using System.ServiceModel;
using Interfaces.Services;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace FileCabinetsService
{
    public class WcfCoreService : AuthenticatingWcfService, IWcfFileCabinetsService
    {
        private IFileCabinetsService _appService;
        private IWcfDocumentsService _documentsService;

        public WcfCoreService(IFileCabinetsService appService,
            IWcfAuthenticationService authService, IWcfDocumentsService documentsService) : base(authService)
        {
            _appService = appService;
            _documentsService = documentsService;
        }

        public FileCabinet GetFileCabinetById(string guid)
        {
            EnsureIsAuthorized();

            var fc = _appService.GetFileCabinetById(Guid.Parse(guid));
            return fc;
        }

        public IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery)
        {
            EnsureIsAuthorized();

            var fcs = _appService.GetFileCabinets(searchQuery);
            return fcs;
        }

        public int GetCount(SearchQuery searchQuery)
        {
            EnsureIsAuthorized();

            int count = _appService.GetCount(searchQuery);
            return count;
        }

        public FileCabinet Add(FileCabinet fileCabinet)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            _appService.Add(fileCabinet);
            return fileCabinet;
        }

        public void Delete(string guid)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            _appService.Delete(Guid.Parse(guid));

            var cookieHeader = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Cookie];

            var documentsInFc = WrapFuncInNewContext((IContextChannel)_documentsService,
                _documentsService.GetDocuments, guid, (SearchQuery)null);

            var documentGuids = documentsInFc.Select(doc => doc.Guid.ToString());
            WrapActionInNewContext((IContextChannel)_documentsService, _documentsService.Delete, documentGuids);
        }

        public void Update(FileCabinet updated)
        {
            EnsureIsAuthorized();
            EnsureIsNotGuest();

            _appService.Update(updated);
        }
    }
}
